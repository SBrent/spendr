QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Calculators/AmortizationCalculator/amortizationcalculator.cpp \
    Calculators/CompoundInterestCalculator/compoundinterestcalculator.cpp \
    Calculators/DownPaymentCalculator/downpaymentcalculator.cpp \
    Calculators/PresentValueCalculator/presentvaluecalculator.cpp \
    Calculators/RentVsBuyCalculator/rentvbuycalculator.cpp \
    DB/database.cpp \
    DB/dbnetworth.cpp \
    UI/amortizationUI.cpp \
    UI/compoundinterestUI.cpp \
    UI/downpaymentUI.cpp \
    UI/presentvalueUI.cpp \
    UI/rentvbuyUI.cpp \
    UI/main.cpp \
    UI/mainwindow.cpp \

HEADERS += \
    Calculators/AmortizationCalculator/amortizationcalculator.h \
    Calculators/CompoundInterestCalculator/compoundinterestcalculator.h \
    Calculators/DownPaymentCalculator/downpaymentcalculator.h \
    Calculators/PresentValueCalculator/presentvaluecalculator.h \
    Calculators/RentVsBuyCalculator/rentvbuycalculator.h \
    DB/database.hpp \
    DB/dbnetworth.hpp \
    UI/amortizationUI.h \
    UI/compoundinterestUI.h \
    UI/downpaymentUI.h \
    UI/presentvalueUI.h \
    UI/rentvbuyUI.h \
    UI/mainwindow.h \

FORMS += \
    UI/mainwindow.ui \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    Calculators/PresentValueCalculator/.gitkeep
