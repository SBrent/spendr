#include <cmath>
#include "amortizationcalculator.h"

double AmortizationCalculator::AmortizationCalc() {

    //Convert interest rate and loan term to monthly
    double monthlyInterestRate = m_interestRate / 12;
    int numOfPayments = m_loanTerm * 12;

    //Calculate monthly payment amount
    double monthlyPayment;
    monthlyPayment = m_loanAmount * ((monthlyInterestRate * pow((1.0 + monthlyInterestRate),
            numOfPayments)) / (pow((1.0 + monthlyInterestRate),numOfPayments) - 1.0));

    //Calculate amortization schedule amounts
    m_beginningBalanceAmounts.clear();
    m_interestAmounts.clear();
    m_principalAmounts.clear();
    m_endingBalanceAmounts.clear();

    double currentBeginningBalanceAmount = 0.0;
    double currentInterestAmount = 0.0;
    double currentPrincipalAmount = 0.0;
    double currentEndingBalanceAmount = 0.0;

    for (int count = 0; count < numOfPayments; count++) {
        //Calculate the beginning balance for current period
        if (count == 0) {
            //Starting balance is original loan amount
            currentBeginningBalanceAmount = m_loanAmount;
            m_beginningBalanceAmounts.push_back(currentBeginningBalanceAmount);
        }
        else {
            currentBeginningBalanceAmount = currentEndingBalanceAmount;
            m_beginningBalanceAmounts.push_back(currentBeginningBalanceAmount);
        }

        //Calculate interest per current payment
        currentInterestAmount = currentBeginningBalanceAmount * monthlyInterestRate;
        m_interestAmounts.push_back(currentInterestAmount);

        //Calculate principal per current payment
        currentPrincipalAmount = monthlyPayment - currentInterestAmount;
        m_principalAmounts.push_back(currentPrincipalAmount);

        //Calculate ending balance per current payment
        currentEndingBalanceAmount = currentBeginningBalanceAmount - currentPrincipalAmount;
        m_endingBalanceAmounts.push_back(currentEndingBalanceAmount);
    }

    return monthlyPayment;
}




// GETTERS & SETTERS

double AmortizationCalculator::getM_loanAmount() const {
    return m_loanAmount;
}
void AmortizationCalculator::setM_loanAmount(double loanAmount) {
    AmortizationCalculator::m_loanAmount = loanAmount;
}

double AmortizationCalculator::getM_interestRate() const {
    return m_interestRate;
}
void AmortizationCalculator::setM_interestRate(double interestRate) {
    AmortizationCalculator::m_interestRate = interestRate;
}

int AmortizationCalculator::getM_loanTerm() const {
    return m_loanTerm;
}
void AmortizationCalculator::setM_loanTerm(int loanTerm) {
    AmortizationCalculator::m_loanTerm = loanTerm;
}

const vector<double> &AmortizationCalculator::getM_beginningBalanceAmounts() const {
    return m_beginningBalanceAmounts;
}

const vector<double> &AmortizationCalculator::getM_interestAmounts() const {
    return m_interestAmounts;
}

const vector<double> &AmortizationCalculator::getM_principalAmounts() const {
    return m_principalAmounts;
}

const vector<double> &AmortizationCalculator::getM_endingBalanceAmounts() const {
    return m_endingBalanceAmounts;
}


// /*******************************************************************************
// ** Function Description: This function displays the amortization schedule amounts
// ** for monthly beginning balances, interest amounts per month, principal amounts
// ** per month, monthly ending balance given the inputs of vectors that hold these
// ** schedule amounts, loan amount, loan term, and interest rate passed as arguments.
// ** This function returns nothing as it is a void function.
// *******************************************************************************/
// void displayAmortizationSchedule(vector<double>&beginningBalanceAmounts,
        // vector<double>&interestAmounts, vector<double>&principalAmounts,
        // vector<double>&endingBalanceAmounts, double loanAmount, int loanTerm,
        // double monthlyPayment){
    // //Calculate total number of payments and total interest
    // int numOfPayments = loanTerm * 12;
    // double totalOfPayments = monthlyPayment * numOfPayments;
    // double totalInterest = totalOfPayments - loanAmount;
    // //Display all values in schedule
    // cout << endl;
    // cout << "Payment Amount: $" << std::fixed << std::setprecision(2) <<
    // monthlyPayment << endl;
    // cout << "Total of " << numOfPayments << " Loan Payments: $" << std::fixed <<
    // std::setprecision(2) << totalOfPayments << endl;
    // cout << "Total Interest: $" << std::fixed << std::setprecision(2) <<
    // totalInterest << endl;
    // cout << endl;
    // cout << "Monthly Amortization Schedule:\n";
    // cout << "Period   Beginning Balance   Interest   Principal   Ending Balance\n";
    // for(int count = 0; count < numOfPayments; count++){
        // cout << "   " << count + 1 << "         $" << std::fixed << std::setprecision(2) <<
        // beginningBalanceAmounts[count] << "      $" << interestAmounts[count] <<
        // "    $" << principalAmounts[count] << "     $" << endingBalanceAmounts[count]
        // << endl;
    // }
// }

