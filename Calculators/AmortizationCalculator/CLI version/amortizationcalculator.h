#ifndef AMORTIZATIONCALCULATOR_H
#define AMORTIZATIONCALCULATOR_H

#include "commandlinemenu.h"
#include <vector>
#include <iomanip>
#include <cmath>

using std::vector;

double amortizationCalc(vector<double>&, vector<double>&, vector<double>&,
        vector<double>&, double, int, double);
void displayAmortizationSchedule(vector<double>&, vector<double>&, vector<double>&,
        vector<double>&, double, int, double);

#endif AMORTIZATIONCALCULATOR_H
