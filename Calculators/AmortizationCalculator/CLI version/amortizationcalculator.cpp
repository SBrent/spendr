#include "amortizationcalculator.h"

/*******************************************************************************
** Function Description: This function calculates the monthly payment amount
** given the inputs of vectors to hold the schedule amounts, loan amount, loan
** term, and interest rate passed as arguments. This function returns the monthly
** payment amount.
*******************************************************************************/
double amortizationCalc(vector<double>&beginningBalanceAmounts,
        vector<double>&interestAmounts, vector<double>&principalAmounts,
        vector<double>&endingBalanceAmounts, double loanAmount, int loanTerm,
        double interestRate){
    //Convert interest rate and loan term to monthly
    double monthlyInterestRate = interestRate / 12;
    int numOfPayments = loanTerm * 12;
    //Calculate monthly payment amount
    double monthlyPayment;
    monthlyPayment = loanAmount * ((monthlyInterestRate * pow((1.0 + monthlyInterestRate),
            numOfPayments)) / (pow((1.0 + monthlyInterestRate),numOfPayments) - 1.0));
    //Calculate amortization schedule amounts
    double currentBeginningBalanceAmount;
    double currentInterestAmount;
    double currentPrincipalAmount;
    double currentEndingBalanceAmount;
    for(int count = 0; count < numOfPayments; count++){
        //Calculate the beginning balance for current period
        if(count == 0){
            //Starting balance is original loan amount
            currentBeginningBalanceAmount = loanAmount;
            beginningBalanceAmounts.push_back(currentBeginningBalanceAmount);
        }
        else{
            currentBeginningBalanceAmount = currentEndingBalanceAmount;
            beginningBalanceAmounts.push_back(currentBeginningBalanceAmount);
        }
        //Calculate interest per current payment
        currentInterestAmount = currentBeginningBalanceAmount * monthlyInterestRate;
        interestAmounts.push_back(currentInterestAmount);
        //Calculate principal per current payment
        currentPrincipalAmount = monthlyPayment - currentInterestAmount;
        principalAmounts.push_back(currentPrincipalAmount);
        //Calculate ending balance per current payment
        currentEndingBalanceAmount = currentBeginningBalanceAmount -
                currentPrincipalAmount;
        endingBalanceAmounts.push_back(currentEndingBalanceAmount);
    }
    return monthlyPayment;
}

/*******************************************************************************
** Function Description: This function displays the amortization schedule amounts
** for monthly beginning balances, interest amounts per month, principal amounts
** per month, monthly ending balance given the inputs of vectors that hold these
** schedule amounts, loan amount, loan term, and interest rate passed as arguments.
** This function returns nothing as it is a void function.
*******************************************************************************/
void displayAmortizationSchedule(vector<double>&beginningBalanceAmounts,
        vector<double>&interestAmounts, vector<double>&principalAmounts,
        vector<double>&endingBalanceAmounts, double loanAmount, int loanTerm,
        double monthlyPayment){
    //Calculate total number of payments and total interest
    int numOfPayments = loanTerm * 12;
    double totalOfPayments = monthlyPayment * numOfPayments;
    double totalInterest = totalOfPayments - loanAmount;
    //Display all values in schedule
    cout << endl;
    cout << "Payment Amount: $" << std::fixed << std::setprecision(2) <<
    monthlyPayment << endl;
    cout << "Total of " << numOfPayments << " Loan Payments: $" << std::fixed <<
    std::setprecision(2) << totalOfPayments << endl;
    cout << "Total Interest: $" << std::fixed << std::setprecision(2) <<
    totalInterest << endl;
    cout << endl;
    cout << "Monthly Amortization Schedule:\n";
    cout << "Period   Beginning Balance   Interest   Principal   Ending Balance\n";
    for(int count = 0; count < numOfPayments; count++){
        cout << "   " << count + 1 << "         $" << std::fixed << std::setprecision(2) <<
        beginningBalanceAmounts[count] << "      $" << interestAmounts[count] <<
        "    $" << principalAmounts[count] << "     $" << endingBalanceAmounts[count]
        << endl;
    }
}