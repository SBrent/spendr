#ifndef AMORTIZATIONCALCULATOR_H
#define AMORTIZATIONCALCULATOR_H

#include <vector>

using std::vector;

class AmortizationCalculator {

private:
    double m_loanAmount = 10000.0;
    double m_interestRate = .04;
    int    m_loanTerm = 10;
    vector<double> m_beginningBalanceAmounts;
    vector<double> m_interestAmounts;
    vector<double> m_principalAmounts;
    vector<double> m_endingBalanceAmounts;

public:
    double AmortizationCalc();

    double getM_loanAmount() const;
    void setM_loanAmount(double loanAmount);
    double getM_interestRate() const;
    void setM_interestRate(double interestRate);
    int getM_loanTerm() const;
    void setM_loanTerm(int loanTerm);
    const vector<double> &getM_beginningBalanceAmounts() const;
    const vector<double> &getM_interestAmounts() const;
    const vector<double> &getM_principalAmounts() const;
    const vector<double> &getM_endingBalanceAmounts() const;

};


#endif // AMORTIZATIONCALCULATOR_H
