#ifndef COMPOUNDINTERESTINPUTVALIDATION_H
#define COMPOUNDINTERESTINPUTVALIDATION_H

#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

int convertStringToInt(string);
double convertStringToDouble(string);

#endif
