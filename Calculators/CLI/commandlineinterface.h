#ifndef COMMANDLINEINTERFACE_H
#define COMMANDLINEINTERFACE_H

#include "compoundinterestcalc.h"
#include "rentvbuycalculator.h"
#include "mortgagepaymentcalc.h"
#include "presentvaluecalc.h"
#include "amortizationcalc.h"

const int MAX = 100;
const int MIN = 1;
const double MAXD = 2147483647.0;
const double MIND = 1.0;

class CommandLineInterface{
private:
    int menuChoice;
    string startPrompt;
    string quitPrompt;
    string determineCalcPrompt;
    string compoundInterestCalcPrompt;
    string rentVsBuyCalcPrompt;
    string netWorthCalcPrompt;
    string downPaymentCalcPrompt;
    string presentValueCalcPrompt;
    string amortizationCalcPrompt;
    void selectCompoundInterestCalc(Menu *);
    void selectRentVsBuyCalc(Menu *);
    void selectNetWorthCalc(Menu *);
    void selectDownPaymentCalc(Menu *);
    void selectPresentValueCalc(Menu *);
    void selectAmortizationCalc(Menu *);
public:
    CommandLineInterface();
    ~CommandLineInterface();
    void startInterface();
};

#endif
