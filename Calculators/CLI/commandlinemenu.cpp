#include "commandlinemenu.h"

/*******************************************************************************
** Function Description: This function is a default constructor for the Menu
** class that initializes each data member. This function returns nothing as it
** is a constructor.
*******************************************************************************/
Menu::Menu(){
   userDefinedInput = "";
   validatedInteger = 0;
   validatedDouble = 0.0;
   outOfRangeMessage = "The input you entered is out of the acceptable range. "
                       "Try again.\n";
}

/*******************************************************************************
** Function Description: This function is a destructor for the Menu class. This
** function returns nothing as it is a destructor.
*******************************************************************************/
Menu::~Menu(){
}

/*******************************************************************************
** Function Description: This function provides displays an interface between
** the user and the program for user input. It displays two strings passed a
** parameters and uses two ints for min and max range for validation range. It
** then gets input from the user and validates it with the convertToInt
** function. This function returns an int so it can be used by the calling
** program for user choices.
*******************************************************************************/
int Menu::userIntChoice(const string &displayPromptOne, const string
                        &displayPromptTwo, int min, int max){
    do{
        cout << displayPromptOne << endl;
        cout << displayPromptTwo << endl;
        getline(cin, userDefinedInput);
        validatedInteger = convertStringToInt(userDefinedInput);
        if(validatedInteger < min || validatedInteger > max){
            cout << outOfRangeMessage;
        }
    }while(validatedInteger < min || validatedInteger> max);
    return validatedInteger;
}

/*******************************************************************************
** Function Description: This function provides displays an interface between
** the user and the program for user input. It displays two strings passed a
** parameters and uses three ints for min and max range for validation range. It
** then gets input from the user and validates it with the convertToInt
** function. This function returns an int so it can be used by the calling
** program for user choices.
*******************************************************************************/
int Menu::userIntChoice(const string &displayPromptOne, const string
                        &displayPromptTwo, const string &displayPromptThree,
                        int min, int max){
    do{
        cout << displayPromptOne << endl;
        cout << displayPromptTwo << endl;
        cout << displayPromptThree << endl;
        getline(cin, userDefinedInput);
        validatedInteger = convertStringToInt(userDefinedInput);
        if(validatedInteger < min || validatedInteger > max){
            cout << outOfRangeMessage;
        }
    }while(validatedInteger < min || validatedInteger> max);
    return validatedInteger;
}

/*******************************************************************************
** Function Description: This function provides displays an interface between
** the user and the program for user input. It displays seven strings passed a
** parameters and uses two ints for min and max range for validation range. It
** then gets input from the user and validates it with the convertToInt
** function. This function returns an int so it can be used by the calling
** program for user choices.
*******************************************************************************/
int Menu::userIntChoice(const string &displayPromptOne, const string
    &displayPromptTwo, const string &displayPromptThree, const string
    &displayPromptFour, const string &displayPromptFive, const string
    &displayPromptSix, const string &displayPromptSeven, int min, int max){
    do{
        cout << displayPromptOne << endl;
        cout << displayPromptTwo << endl;
        cout << displayPromptThree << endl;
        cout << displayPromptFour << endl;
        cout << displayPromptFive << endl;
        cout << displayPromptSix << endl;
        cout << displayPromptSeven << endl;
        getline(cin, userDefinedInput);
        validatedInteger = convertStringToInt(userDefinedInput);
        if(validatedInteger < min || validatedInteger > max){
            cout << outOfRangeMessage;
        }
    }while(validatedInteger < min || validatedInteger > max);
    return validatedInteger;
}

/*******************************************************************************
** Function Description: This function provides displays an interface between
** the user and the program for user input. It displays five strings passed a
** parameters and uses two ints for min and max range for validation range. It
** then gets input from the user and validates it with the convertToInt
** function. This function returns an int so it can be used by the calling
** program for user choices.
*******************************************************************************/
int Menu::userIntChoice(const string &displayPromptOne, const string
    &displayPromptTwo, const string &displayPromptThree, const string
    &displayPromptFour, const string &displayPromptFive, int min, int max){
    do{
        cout << displayPromptOne << endl;
        cout << displayPromptTwo << endl;
        cout << displayPromptThree << endl;
        cout << displayPromptFour << endl;
        cout << displayPromptFive << endl;
        getline(cin, userDefinedInput);
        validatedInteger = convertStringToInt(userDefinedInput);
        if(validatedInteger < min || validatedInteger > max){
            cout << outOfRangeMessage;
        }
    }while(validatedInteger < min || validatedInteger > max);
    return validatedInteger;
}


/*******************************************************************************
** Function Description: This function provides displays an interface between
** the user and the program for user input. It displays one string passed a
** parameter and uses two ints for min and max range for validation range. It
** then gets input from the user and validates it with the convertToInt
** function. This function returns an int so it can be used by the calling
** program for user choices.
*******************************************************************************/
int Menu::userIntChoice(const string &displayPromptOne, int min, int max){
    do{
        cout << displayPromptOne << endl;
        getline(cin, userDefinedInput);
        validatedInteger = convertStringToInt(userDefinedInput);
        if(validatedInteger < min || validatedInteger > max){
            cout << outOfRangeMessage;
        }
    }while(validatedInteger < min || validatedInteger> max);
    return validatedInteger;
}

/*******************************************************************************
** Function Description: This function provides displays an interface between
** the user and the program for user input. It displays one string passed a
** parameter and uses two ints for min and max range for validation range. It
** then gets input from the user and validates it with the convertToDouble
** function. This function returns an double so it can be used by the calling
** program for user choices.
*******************************************************************************/
double Menu::userDoubleChoice(const string &displayPrompt, double min, double max){
    do{
        cout << displayPrompt << endl;
        getline(cin, userDefinedInput);
        validatedDouble = convertStringToDouble(userDefinedInput);
        if(validatedDouble < min || validatedDouble > max){
            cout << outOfRangeMessage;
        }
    }while(validatedDouble < min || validatedDouble > max);
    return validatedDouble;
}
