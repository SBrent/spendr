#ifndef COMPOUNDINTERESTMENU_H
#define COMPOUNDINTERESTMENU_H

#include "commandlineinputvalidation.h"

using std::cin;

class Menu{
private:
    string userDefinedInput;
    int validatedInteger;
    double validatedDouble;
    string outOfRangeMessage;
public:
    Menu();
    ~Menu();
    int userIntChoice(const string &, int, int);
    int userIntChoice(const string &, const string &, int, int);
    int userIntChoice(const string &, const string &, const string &, int , int);
    int userIntChoice(const string &, const string &, const string &, const
    string &, const string &, const string &, const string &, int, int);
    int userIntChoice(const string &, const string &, const string &, const
    string &, const string &, int, int);
    double userDoubleChoice(const string&, double, double);
};

#endif
