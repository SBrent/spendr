#include "commandlineinputvalidation.h"

/*******************************************************************************
** Function Description: convertStringToInt takes a string variable as an
** argument in order to read in a string entered by a user and then converts the
** string to an integer within the defined range. If invalid input exists in the
** string then the user is prompted to enter a new string until the string
** consists of only digits so it can convert to an integer. This function
** returns an int as this will be used to select a choice and/or define a menu
** selection.
*******************************************************************************/
int convertStringToInt(string inputIn){
    int numOfInvalidIntegers = 0;
    int result = -1;
    int currentPositionInString = 0;
    string invalidInputPrompt = "You have entered invalid input. Try Again.";

    //Determine if string is empty because user only hit enter
    if(inputIn.length() == 0){
        numOfInvalidIntegers++;
    }
    else {
        //Determine is each char in string is a valid digit
        for(unsigned count = 0; count < inputIn.length(); count++){
            currentPositionInString = inputIn.at(count);
            if(!isdigit(currentPositionInString)){
                numOfInvalidIntegers++;
            }
        }
    }
    //Determine the number derived from the string if each char is a valid digit
    if(numOfInvalidIntegers == 0){
        result = stoi(inputIn); //Referenced material at cplusplus.com for stoi
                                // usage
    }
    //Return an invalid negative integer to represent that the string had an
    //invalid char in it
    else{
        cout << invalidInputPrompt << endl;
        return result;
    }
    return result;
}

/*******************************************************************************
** Function Description: convertStringToDouble takes a string variable as an
** argument in order to read in a string entered by a user and then converts the
** string to an double within the defined range. If invalid input exists in the
** string then the user is prompted to enter a new string until the string
** consists of only digits or decimal point so it can convert to an integer.
** This function returns an double as this will be used to select a choice and/or
** define a menu selection.
*******************************************************************************/
double convertStringToDouble(string inputIn){
    int numOfInvalidIntegers = 0;
    double result = -1.0;
    int currentPositionInString = 0;
    string invalidInputPrompt = "You have entered invalid input. Try Again.";

    //Determine if string is empty because user only hit enter
    if(inputIn.length() == 0){
        numOfInvalidIntegers++;
    }
    else {
        //Determine is each char in string is a valid digit
        for(unsigned count = 0; count < inputIn.length(); count++){
            currentPositionInString = inputIn.at(count);
            if(!isdigit(currentPositionInString) && currentPositionInString != 46){
                numOfInvalidIntegers++;
            }
        }
    }
    //Determine the number derived from the string if each char is a valid digit
    if(numOfInvalidIntegers == 0){
        result = stod(inputIn);
    }
    //Return an invalid negative double to represent that the string had an
    //invalid char in it
    else{
        cout << invalidInputPrompt << endl;
        return result;
    }
    return result;
}
