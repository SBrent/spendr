#include "commandlineinterface.h"

/*******************************************************************************
** Function Description: This function is a default constructor for the Interface
** class. It assigns default values for many data members to start the interface.
** This function returns nothing as it is a constructor.
*******************************************************************************/
CommandLineInterface::CommandLineInterface(){
    menuChoice = 0;
    startPrompt = "1. Run a Calculation";
    quitPrompt = "2. Exit";
    determineCalcPrompt = "Enter the number of the calculator below you would"
                          " like to use:";
    compoundInterestCalcPrompt = "1: Compound Interest Calculator";
    rentVsBuyCalcPrompt = "2: Rent vs Buy Calculator";
    netWorthCalcPrompt = "3: Net Worth Calculator";
    downPaymentCalcPrompt = "4: Down Payment Calculator";
    presentValueCalcPrompt = "5: Present Value Calculator";
    amortizationCalcPrompt = "6: Amortization Calculator";
}

/*******************************************************************************
** Function Description: This function is a default destructor for the Interface
** class. This function returns nothing as it is a destructor.
*******************************************************************************/
CommandLineInterface::~CommandLineInterface(){
}

/*******************************************************************************
** Function Description: This function implements the financial calculators.
** It uses the menu class to prompt user for starting or ending the interface.
** If start is chosen, the user enters the inputs for the function, then the
** results of the calculation is displayed. This function returns nothing as it
** is a void function.
*******************************************************************************/
void CommandLineInterface::startInterface(){
    //Display menu and run until user specifies they would like to quit
    Menu currentInterfaceMenu;

    //Iterate until user decides to quit
    do{
        //Get user choice on which calculator to use
        cout << endl;
        menuChoice = currentInterfaceMenu.userIntChoice(determineCalcPrompt,
                compoundInterestCalcPrompt, rentVsBuyCalcPrompt,
                netWorthCalcPrompt, downPaymentCalcPrompt, presentValueCalcPrompt,
                amortizationCalcPrompt, MIN, 6);
        switch(menuChoice){
            case 1: selectCompoundInterestCalc(&currentInterfaceMenu);
                    break;
            case 2: selectRentVsBuyCalc(&currentInterfaceMenu);
                    break;
            case 3: selectNetWorthCalc(&currentInterfaceMenu);
                    break;
            case 4: selectDownPaymentCalc(&currentInterfaceMenu);
                    break;
            case 5: selectPresentValueCalc(&currentInterfaceMenu);
                    break;
            case 6: selectAmortizationCalc(&currentInterfaceMenu);
                    break;
            default:;
        }

        //Ask user to start another calculation or quit
        cout << endl;
        menuChoice = currentInterfaceMenu.userIntChoice(startPrompt, quitPrompt,
                MIN, 2);
    }while(menuChoice != 2);
}

void CommandLineInterface::selectCompoundInterestCalc(Menu *currentInterfaceMenu){
    cout << endl;
    cout << "Compound Interest Calculator Selected\n";
    //Get user input for calling compound interest calculator function
    string principalPrompt = "Enter dollar value of initial investment:";
    string interestRatePrompt = "Enter estimated annual interest rate(you may "
                                "include decimal point):";
    string lengthOfTimeInYearsPrompt = "Enter the number of years you want to "
                                       "invest(max of 100):";
    string compoundFrequencyPrompt = "Enter the number of selection below for "
                                     "times per year that interest will be "
                                     "compounded:";
    string semiAnnualCompoundingPrompt = "1. Semi-annual";
    string monthlyCompoundingPrompt = "2. Monthly";
    string dailyCompoundingPrompt = "3. Daily";
    string annualCompoundingPrompt = "4: Annual";
    double principal;
    double interestRate;
    int lengthOfTimeInvested;
    int compoundFrequencyChoice;
    vector<double>listOfFutureValues;

    principal = currentInterfaceMenu->userDoubleChoice(principalPrompt, MIND,
            MAXD);
    interestRate = (currentInterfaceMenu->userDoubleChoice(interestRatePrompt,
            MIN, MAX) / 100.0);
    lengthOfTimeInvested = currentInterfaceMenu->userIntChoice(
            lengthOfTimeInYearsPrompt, MIN, MAX);
    compoundFrequencyChoice = currentInterfaceMenu->userIntChoice(
            compoundFrequencyPrompt, semiAnnualCompoundingPrompt,
            monthlyCompoundingPrompt, dailyCompoundingPrompt,
            annualCompoundingPrompt, MIN, 4);

    //Call compoundInterestCalc function and display results
    compoundInterestCalc(listOfFutureValues, principal, interestRate,
            lengthOfTimeInvested, compoundFrequencyChoice);
    displayListOfFutureValues(listOfFutureValues, compoundFrequencyChoice);
}

void CommandLineInterface::selectRentVsBuyCalc(Menu *currentInterfaceMenu){
    cout << endl;
    cout << "Rent vs Buy Calculator Selected\n";
    //Get user input to update
    RentVBuyCalculator newCalculation;
    double numBuffer = 0.0;
    string purchasePricePrompt = "Enter Purchase Price: $";
    string downPaymentPercentagePrompt = "Enter Down Payment Percent (%):";
    string buyingClosingCostPercentagePrompt = "Buying Closing Cost (%):";
    string loanTermPrompt = "Loan Term:";
    string mortgageRatePercentagePrompt = "Mortgage Rate (%):";
    string homeAppreciationRatePercentagePrompt = "Home Appreciation Rate (%):";
    string propertyTaxRatePercentagePrompt = "Property Tax Rate (%):";
    string annualInsurancePrompt = "Annual Insurance: $";
    string annualMaintenancePrompt = "Annual Maintenance: $";
    string sellerClosingCostsPercentagePrompt = "Selling Closing Cost (%):";
    string lostInvestmentOpportunityRatePercentagePrompt = "Lost Investment "
                                                           "Opportunity Rate (%):";
    string monthlyRentPrompt = "Monthly Rent: $";
    string securityDepositPrompt = "Security Deposit: $";
    string rentalIncreaseRatePrompt = "Rental Increase Rate (%):";
    string numberOfYearsPrompt = "Number of Years to Look Ahead:";

    numBuffer = currentInterfaceMenu->userDoubleChoice(purchasePricePrompt, MIND, MAXD);
    newCalculation.setM_purchasePrice(numBuffer);
    numBuffer = currentInterfaceMenu->userDoubleChoice(downPaymentPercentagePrompt, MIND, MAX);
    newCalculation.setM_downPaymentPercent(numBuffer/100.0);
    numBuffer = currentInterfaceMenu->userDoubleChoice(buyingClosingCostPercentagePrompt, MIND, MAX);
    newCalculation.setM_buyingClosingCost(numBuffer/100.0);
    numBuffer = currentInterfaceMenu->userIntChoice(loanTermPrompt, MIN, MAX);
    newCalculation.setM_loanTerm(numBuffer);
    numBuffer = currentInterfaceMenu->userDoubleChoice(mortgageRatePercentagePrompt, MIND, MAX);
    newCalculation.setM_mortgageRate(numBuffer/100.0);
    numBuffer = currentInterfaceMenu->userDoubleChoice(homeAppreciationRatePercentagePrompt, MIND, MAX);
    newCalculation.setM_homeAppreciationRate(numBuffer/100.0);
    numBuffer = currentInterfaceMenu->userDoubleChoice(propertyTaxRatePercentagePrompt, MIND, MAX);
    newCalculation.setM_propertyTaxRate(numBuffer/100.0);
    numBuffer = currentInterfaceMenu->userDoubleChoice(annualInsurancePrompt, MIND, MAXD);
    newCalculation.setM_annualInsurance(numBuffer);
    numBuffer = currentInterfaceMenu->userDoubleChoice(annualMaintenancePrompt, MIND, MAXD);
    newCalculation.setM_annualMaintenance(numBuffer);
    numBuffer = currentInterfaceMenu->userDoubleChoice(sellerClosingCostsPercentagePrompt, MIND, MAX);
    newCalculation.setM_sellingClosingCost(numBuffer/100.0);
    numBuffer = currentInterfaceMenu->userDoubleChoice(lostInvestmentOpportunityRatePercentagePrompt, MIND, MAX);
    newCalculation.setM_investmentRate(numBuffer/100.0);
    numBuffer = currentInterfaceMenu->userDoubleChoice(monthlyRentPrompt, MIND, MAXD);
    newCalculation.setM_monthlyRent(numBuffer);
    numBuffer = currentInterfaceMenu->userDoubleChoice(securityDepositPrompt, MIND, MAXD);
    newCalculation.setM_securityDeposit(numBuffer);
    numBuffer = currentInterfaceMenu->userDoubleChoice(rentalIncreaseRatePrompt, MIND, MAX);
    newCalculation.setM_rentalIncreaseRate(numBuffer/100.0);
    numBuffer = currentInterfaceMenu->userIntChoice(numberOfYearsPrompt, MIN, MAX);
    newCalculation.setM_nYears(numBuffer);

    // report results
    cout << "\nAfter " << newCalculation.getM_nYears() << " years...\n";
    auto buyCost = newCalculation.TotalBuyingCostsAfterN();
    auto rentCost = newCalculation.TotalRentingCostsAfterN();
    std::ios init(NULL);
    init.copyfmt(std::cout);                                                 // save default cout formatting
    cout << std::fixed << std::showpoint << std::setprecision(2);       // use dollar & cents formatting
    // display each cost as total over n years & average monthly cost
    cout << "Total Buying Costs: \t$"  << buyCost << " (avg $"
              << buyCost/(newCalculation.getM_nYears()*12) << "/mo)\n";
    cout << "Total Renting Costs: \t$" << rentCost << " (avg $"
              << rentCost/(newCalculation.getM_nYears()*12) << "/mo)\n";
    cout.copyfmt(init);                                                 // restore default formatting
    // display rent/buy decision
    cout << std::noshowpoint << "Less Expensive Option: \t"
              << ((buyCost < rentCost) ? "BUY" : "RENT") << "\n\n";
}

void CommandLineInterface::selectNetWorthCalc(Menu *currentInterfaceMenu){
    cout << endl;
    cout << "Net Worth Calculator Selected\n";
    cout << "This calculator requires Qt software install and has not been fully"
            "implemented with this command line interface yet\n";
}

void CommandLineInterface::selectDownPaymentCalc(Menu *currentInterfaceMenu){
    cout << endl;
    cout << "Down Payment Calculator Selected\n";
    startCalculator(currentInterfaceMenu);
}

void CommandLineInterface::selectPresentValueCalc(Menu *currentInterfaceMenu){
    cout << endl;
    cout << "Present Value Calculator Selected\n";
    //Get user input for calling presentValueCalc
    double futureValue;
    int numOfPeriods;
    double interestRate;
    double totalInterest;
    double presentValue;
    string futureValuePrompt = "Enter the dollar value of investment to be "
                               "received in the future:";
    string numOfPeriodsPrompt = "Enter the number of periods(max of 100):";
    string interestRatePrompt = "Enter estimated annual interest rate(you may"
                                " include decimal point):";

    futureValue = currentInterfaceMenu->userDoubleChoice(futureValuePrompt, MIND, MAXD);
    numOfPeriods = currentInterfaceMenu->userIntChoice(numOfPeriodsPrompt, MIN, MAX);
    interestRate = (currentInterfaceMenu->userDoubleChoice(interestRatePrompt, MIND, MAX) / 100.0);

    //Call presentValueCalc and display results
    presentValue = presentValueCalc(futureValue, numOfPeriods, interestRate);
    totalInterest = futureValue - presentValue;

    cout << endl;
    cout << "Present Value: $" << std::fixed << std::setprecision(2) << presentValue << endl;
    cout << "Total Interest: $" << std::fixed << std::setprecision(2) << totalInterest << endl;
}

void CommandLineInterface::selectAmortizationCalc(Menu *currentInterfaceMenu){
    cout << endl;
    cout << "Amortization Calculator Selected\n";
    //Get user input for calling amortizationCalc
    double loanAmount;
    int loanTerm;
    double interestRate;
    vector<double>beginningBalanceAmounts;
    vector<double>interestAmounts;
    vector<double>principalAmounts;
    vector<double>endingBalanceAmounts;
    double monthlyPayment;
    string loanAmountPrompt = "Enter the dollar value of the loan amount:";
    string loanTermPrompt = "Enter the loan term(max of 100)";
    string interestRatePrompt = "Enter the estimated annual interest rate(you may"
                                " include the decimal point):";

    loanAmount = currentInterfaceMenu->userDoubleChoice(loanAmountPrompt, MIND, MAXD);
    loanTerm = currentInterfaceMenu->userIntChoice(loanTermPrompt, MIN, MAX);
    interestRate = (currentInterfaceMenu->userDoubleChoice(interestRatePrompt, MIND, MAX) / 100.0);

    //Call amortizationCalc and display results
    monthlyPayment = amortizationCalc(beginningBalanceAmounts, interestAmounts,
            principalAmounts, endingBalanceAmounts, loanAmount, loanTerm,
            interestRate);
    displayAmortizationSchedule(beginningBalanceAmounts, interestAmounts,
            principalAmounts, endingBalanceAmounts, loanAmount, loanTerm,
            monthlyPayment);
}
