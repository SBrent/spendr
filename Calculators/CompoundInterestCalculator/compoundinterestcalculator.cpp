#include "compoundinterestcalculator.h"

#include <cmath>
#include <string>
#include <vector>

/*******************************************************************************
** Function Description: This function calculates the future values given the
** inputs of principal, interest rate, length of time invested, and choice of
** compounding per year passed as arguments. This function returns nothing as it
** is a void function.
*******************************************************************************/
std::vector<double> CompoundInterestCalculator::compoundInterestCalc(double principal,
                          double interestRate, int numberOfYears, int frequency) {
    std::vector<double> futureValues;
    //Calculate the future value for each year and insert into listOfFutureValues
    for(int count = 0; count <= numberOfYears; count++){
        //Year 0 only includes the principal amount
        if(count == 0){
            futureValues.push_back(principal);
        }
        else{
            //Calculate the current year's compounded interest and principal
            double currentYearCompoundedInterest = 0.0;
                currentYearCompoundedInterest = principal * pow(1.0 +
                        (interestRate / frequency),
                        frequency * count);
            futureValues.push_back(currentYearCompoundedInterest);
        }
    }

    return futureValues;
}
