#ifndef COMPOUNDINTERESTCALC_H
#define COMPOUNDINTERESTCALC_H

#include <vector>

class CompoundInterestCalculator {

public:
    std::vector<double> compoundInterestCalc(double principal, double interestRate, int nYears, int frequency);

};

#endif
