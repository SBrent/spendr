#include <string>
#include <iostream>
#include "stdio.h"
#include "stdlib.h"

using namespace std; 

int showMenu();
int inputMortgageAmt();
void calcUsingDefault(int); 
void calcUsingCustom(int); 

int main(){

    int mortgage;
    int menuChoice;

    menuChoice = showMenu();
    mortgage = inputMortgageAmt();

    switch(menuChoice){
        case 1: 
            calcUsingDefault(mortgage);
            break;
        case 2: 
            calcUsingCustom(mortgage);
            break;
    }

    return 0;
}

//Show menu and get option user selects
int showMenu(){

    int userInput; 

    //Menu prompts
    printf("MORTGAGE PAYMENT CALCULATOR\n");
    printf("Please choose from the menu below\n");
    printf("1. Calculate by default percentages\n");
    printf("2. Calculate by custom percentage\n"); 
    printf("Option: "); 

    //Get user information
    cin >> userInput; 
    while(userInput < 1 || userInput > 2){
        printf("Please choose a valid option\n"); 
        cin.clear();
        cin.ignore();
        cin >> userInput; 
    }

    printf("You chose option: %d\n", userInput); 

    return userInput;
}

//Gets the user to input what mortgage amount they want to use during calculation
int inputMortgageAmt(){

    int mortgage;
    printf("Enter mortgage amount ($0 - $20000000): \n");    
    
    cin >> mortgage;
    while(mortgage < 0 || mortgage > 20000000){
        printf("Please choose between $0 and $20000000\n");
        cin.clear();
        cin.ignore();
        cin >> mortgage;
    }

    return mortgage; 
}

//Calculates mortgage amount based on default values
void calcUsingDefault(int mortgage){
    
    const int defaultRate1 = 5;
    const int defaultRate2 = 10;
    const int defaultRate3 = 20;

    double rate1Amount;
    double rate2Amount;
    double rate3Amount;

    //Calculate with default percentages 
    rate1Amount = mortgage * (defaultRate1 / 100.0);
    rate2Amount = mortgage * (defaultRate2 / 100.0);
    rate3Amount = mortgage * (defaultRate3 / 100.0);
    
    printf("Mortgage Amount: %d\n", mortgage); 
    printf("Default Percentages:\n");
    printf("%d%%: $%.2f", defaultRate1, rate1Amount);
    printf("\n");
    printf("%d%%: $%.2f", defaultRate2, rate2Amount);
    printf("\n");
    printf("%d%%: $%.2f", defaultRate3, rate3Amount);
    printf("\n");
}

//Calculates using a custom percentage
void calcUsingCustom(int mortgage){

    double customAmount;
    int customRate;

    printf("Please enter a custom rate percentage as an integer.\n");

    cin >> customRate; 
    if(customRate < 0){
        printf("Please enter a valid percentage\n");
        calcUsingCustom(mortgage); 
    }

    //Calculate with default percentages 
    customAmount = mortgage * (customRate / 100.0);
    
    printf("Mortgage Amount: %d\n", mortgage); 
    printf("Custom Percentage: \n");
    printf("%d%%: $%.2f\n", customRate, customAmount);
}
