#ifndef DOWNPAYMENTCALCULATOR_H
#define DOWNPAYMENTCALCULATOR_H

class DownPaymentCalculator {

public:
    double DownPayment();

    // Getters & Setters
    double getM_purchasePrice() const;
    void setM_purchasePrice(double m_purchasePrice);
    double getM_downPaymentPercent() const;
    void setM_downPaymentPercent(double m_downPaymentPercent);

private:  
    double m_purchasePrice = 600000;
    double m_downPaymentPercent = 0.20;

};

#endif // DOWNPAYMENTCALCULATOR_H
