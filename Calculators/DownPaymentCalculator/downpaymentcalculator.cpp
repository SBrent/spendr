#include <cmath>
#include "downpaymentcalculator.h"


// returns downPayment
double DownPaymentCalculator::DownPayment() {
    return m_purchasePrice * m_downPaymentPercent;
}


// GETTERS & SETTERS
double DownPaymentCalculator::getM_purchasePrice() const {
   return m_purchasePrice;
}
void DownPaymentCalculator::setM_purchasePrice(double purchasePrice) {
    DownPaymentCalculator::m_purchasePrice = purchasePrice;
}

double DownPaymentCalculator::getM_downPaymentPercent() const {
    return m_downPaymentPercent;
}
void DownPaymentCalculator::setM_downPaymentPercent(double downPaymentPercent) {
    DownPaymentCalculator::m_downPaymentPercent = downPaymentPercent;
}

