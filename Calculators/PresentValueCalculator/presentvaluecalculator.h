#ifndef PRESENTVALUECALC_H
#define PRESENTVALUECALC_H

class PresentValueCalculator {

public:
    double presentValueCalc(double, int, double);

};

#endif
