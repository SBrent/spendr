#include "presentvaluecalculator.h"

#include <cmath>

/*******************************************************************************
** Function Description: This function calculates the present value given the
** inputs of future value, interest rate, and number of periods passed as
** arguments. This function returns the present value calculated.
*******************************************************************************/
double PresentValueCalculator::presentValueCalc(double futureValue, int numOfPeriods, double interestRate) {
    return futureValue / pow((1.0 + interestRate),numOfPeriods);
}
