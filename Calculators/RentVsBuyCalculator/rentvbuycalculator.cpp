#include <cmath>
#include "rentvbuycalculator.h"


// returns down payment amount given purchase price and percent down (as decimal)
double RentVBuyCalculator::DownPayment() {
    return m_purchasePrice * m_downPaymentPercent;
}


// returns appreciated home value after n years
double RentVBuyCalculator::AppreciatedHomeValueAfterN() {
    return m_purchasePrice * pow(1.0 + m_homeAppreciationRate, m_nYears);
}


// returns mortgage balance (amount still owed) after n years
// formula from: https://mtgprofessor.com/formulas.htm
double RentVBuyCalculator::MortgageBalanceAfterN() {
    auto monthlyMortgageRate = m_mortgageRate / 12;
    auto numPaymentsTotal = m_loanTerm * 12;
    auto numPaymentsAfterN = m_nYears * 12;
    auto initialMortgageBalance = m_purchasePrice - DownPayment();
    return initialMortgageBalance *
           (pow(1.0 + monthlyMortgageRate, numPaymentsTotal) - pow(1.0 + monthlyMortgageRate, numPaymentsAfterN)) /
           (pow(1.0 + monthlyMortgageRate, numPaymentsTotal) - 1);
}


// returns net gain/loss after sale of home after n years
double RentVBuyCalculator::NetFromSale() {
    auto currentHomeVal = AppreciatedHomeValueAfterN();
    return currentHomeVal - MortgageBalanceAfterN() - (m_sellingClosingCost * currentHomeVal);
}


// returns total one-time, up-front costs
double RentVBuyCalculator::UpFrontCosts() {
    return DownPayment() + (m_purchasePrice * m_buyingClosingCost);
}


// returns total property tax after n years (assumes assessed annually & on full current value of home)
double RentVBuyCalculator::PropertyTaxAfterN() {
    auto apprecBase = 1.0 + m_homeAppreciationRate;
    return ((m_propertyTaxRate * m_purchasePrice * pow(apprecBase, m_nYears)) - (m_propertyTaxRate * m_purchasePrice)) /
           (apprecBase - 1.0) ;
}


// returns total ongoing costs (property tax, insurance, maintenance) after n years
double RentVBuyCalculator::OngoingCostsAfterN() {
    return PropertyTaxAfterN() + (m_nYears * (m_annualInsurance + m_annualMaintenance));
}


// returns potential earnings if down payment had been invested
double RentVBuyCalculator::InvestmentEarningsAfterN() {
    return (DownPayment() * pow((1 + m_investmentRate), m_nYears)) - DownPayment();
}


// returns total home buying costs after n years (phrased as positive value b/c reported as cost)
double RentVBuyCalculator::TotalBuyingCostsAfterN() {
    return UpFrontCosts() + OngoingCostsAfterN() + InvestmentEarningsAfterN() - NetFromSale();
}


// returns total renting costs after n years (phrased as positive value b/c reported as cost)
double RentVBuyCalculator::TotalRentingCostsAfterN() {
    auto rentIncFactor = m_rentalIncreaseRate + 1.0;
    auto annualRent = m_monthlyRent * 12;
    return m_securityDeposit + ( (annualRent * pow(rentIncFactor, m_nYears) - annualRent) / (rentIncFactor - 1) );
}


// GETTERS & SETTERS
double RentVBuyCalculator::getM_purchasePrice() const {
   return m_purchasePrice;
}
void RentVBuyCalculator::setM_purchasePrice(double purchasePrice) {
    RentVBuyCalculator::m_purchasePrice = purchasePrice;
}

double RentVBuyCalculator::getM_downPaymentPercent() const {
    return m_downPaymentPercent;
}
void RentVBuyCalculator::setM_downPaymentPercent(double downPaymentPercent) {
    RentVBuyCalculator::m_downPaymentPercent = downPaymentPercent;
}

double RentVBuyCalculator::getM_buyingClosingCost() const {
    return m_buyingClosingCost;
}
void RentVBuyCalculator::setM_buyingClosingCost(double buyingClosingCost) {
    RentVBuyCalculator::m_buyingClosingCost = buyingClosingCost;
}

int RentVBuyCalculator::getM_loanTerm() const {
    return m_loanTerm;
}
void RentVBuyCalculator::setM_loanTerm(int loanTerm) {
    RentVBuyCalculator::m_loanTerm = loanTerm;
}

double RentVBuyCalculator::getM_mortgageRate() const {
    return m_mortgageRate;
}
void RentVBuyCalculator::setM_mortgageRate(double mortgageRate) {
    RentVBuyCalculator::m_mortgageRate = mortgageRate;
}

double RentVBuyCalculator::getM_homeAppreciationRate() const {
    return m_homeAppreciationRate;
}
void RentVBuyCalculator::setM_homeAppreciationRate(double homeAppreciationRate) {
    RentVBuyCalculator::m_homeAppreciationRate = homeAppreciationRate;
}

double RentVBuyCalculator::getM_propertyTaxRate() const {
    return m_propertyTaxRate;
}
void RentVBuyCalculator::setM_propertyTaxRate(double propertyTaxRate) {
    RentVBuyCalculator::m_propertyTaxRate = propertyTaxRate;
}

double RentVBuyCalculator::getM_annualInsurance() const {
    return m_annualInsurance;
}
void RentVBuyCalculator::setM_annualInsurance(double annualInsurance) {
    RentVBuyCalculator::m_annualInsurance = annualInsurance;
}

double RentVBuyCalculator::getM_annualMaintenance() const {
    return m_annualMaintenance;
}
void RentVBuyCalculator::setM_annualMaintenance(double annualMaintenance) {
    RentVBuyCalculator::m_annualMaintenance = annualMaintenance;
}

double RentVBuyCalculator::getM_sellingClosingCost() const {
    return m_sellingClosingCost;
}
void RentVBuyCalculator::setM_sellingClosingCost(double sellingClosingCost) {
    RentVBuyCalculator::m_sellingClosingCost = sellingClosingCost;
}

double RentVBuyCalculator::getM_investmentRate() const {
    return m_investmentRate;
}
void RentVBuyCalculator::setM_investmentRate(double investmentRate) {
    RentVBuyCalculator::m_investmentRate = investmentRate;
}

double RentVBuyCalculator::getM_monthlyRent() const {
    return m_monthlyRent;
}
void RentVBuyCalculator::setM_monthlyRent(double m_monthlyRent) {
    RentVBuyCalculator::m_monthlyRent = m_monthlyRent;
}

double RentVBuyCalculator::getM_securityDeposit() const {
    return m_securityDeposit;
}
void RentVBuyCalculator::setM_securityDeposit(double m_securityDeposit) {
    RentVBuyCalculator::m_securityDeposit = m_securityDeposit;
}

double RentVBuyCalculator::getM_rentalIncreaseRate() const {
    return m_rentalIncreaseRate;
}
void RentVBuyCalculator::setM_rentalIncreaseRate(double rentalIncreaseRate) {
    RentVBuyCalculator::m_rentalIncreaseRate = rentalIncreaseRate;
}

double RentVBuyCalculator::getM_nYears() const {
    return m_nYears;
}
void RentVBuyCalculator::setM_nYears(int nYears) {
    RentVBuyCalculator::m_nYears = nYears;
}


