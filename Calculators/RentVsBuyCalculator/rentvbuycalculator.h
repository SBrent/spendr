#ifndef RENTVBUYCALCULATOR_H
#define RENTVBUYCALCULATOR_H

class RentVBuyCalculator {

public:
    double DownPayment();
    double AppreciatedHomeValueAfterN();
    double MortgageBalanceAfterN();
    double NetFromSale();
    double PropertyTaxAfterN();
    double UpFrontCosts();
    double OngoingCostsAfterN();
    double InvestmentEarningsAfterN();
    double TotalBuyingCostsAfterN();
    double TotalRentingCostsAfterN();

    // Getters & Setters
    double getM_purchasePrice() const;
    void setM_purchasePrice(double m_purchasePrice);
    double getM_downPaymentPercent() const;
    void setM_downPaymentPercent(double m_downPaymentPercent);
    double getM_buyingClosingCost() const;
    void setM_buyingClosingCost(double m_buyingClosingRatePercentage);
    double getM_mortgageRate() const;
    void setM_mortgageRate(double m_mortgageRatePercentage);
    int getM_loanTerm() const;
    void setM_loanTerm(int m_loanTerm);
    double getM_homeAppreciationRate() const;
    void setM_homeAppreciationRate(double m_homeAppreciationRatePercentage);
    double getM_propertyTaxRate() const;
    void setM_propertyTaxRate(double m_propertyTaxRatePercentage);
    double getM_annualInsurance() const;
    void setM_annualInsurance(double m_annualInsurance);
    double getM_annualMaintenance() const;
    void setM_annualMaintenance(double m_annualMaintenance);
    double getM_sellingClosingCost() const;
    void setM_sellingClosingCost(double m_sellingClosingRatePercentage);
    double getM_investmentRate() const;
    void setM_investmentRate(double m_investmentRatePercentage);
    double getM_monthlyRent() const;
    void setM_monthlyRent(double m_monthlyRent);
    double getM_securityDeposit() const;
    void setM_securityDeposit(double m_securityDeposit);
    double getM_rentalIncreaseRate() const;
    void setM_rentalIncreaseRate(double m_rentalIncreaseRatePercentage);
    double getM_nYears() const;
    void setM_nYears(int m_nYears);

private:
    double m_purchasePrice = 600000;
    double m_downPaymentPercent = 0.20;
    double m_buyingClosingCost = 0.04;
    int    m_loanTerm = 30;
    double m_mortgageRate = 0.0375;
    double m_homeAppreciationRate = 0.03;
    double m_propertyTaxRate = 0.018;
    double m_annualInsurance = 1500;
    double m_annualMaintenance = 2000;
    double m_sellingClosingCost = 0.06;
    double m_investmentRate = 0.04;

    double m_monthlyRent = 1800;
    double m_securityDeposit = 1800;
    double m_rentalIncreaseRate = 0.02;

    int    m_nYears = 2;
};

#endif // RENTVBUYCALCULATOR_H
