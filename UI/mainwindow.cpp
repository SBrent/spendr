#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "UI/amortizationUI.h"
#include "UI/compoundinterestUI.h"
#include "UI/downpaymentUI.h"
#include "UI/presentvalueUI.h"
#include "UI/rentvbuyUI.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_launchamortization_clicked() {
    AmortizationCalculatorUI amortizationwindow;
    amortizationwindow.setModal(true);
    amortizationwindow.exec();
}

void MainWindow::on_launchdownpayment_clicked() {
    DownPaymentCalculatorUI downpaymentwindow;
    downpaymentwindow.setModal(true);
    downpaymentwindow.exec();
}

void MainWindow::on_launchrentvbuy_clicked() {
    RentVBuyCalculatorUI rentvbuywindow;
    rentvbuywindow.setModal(true);
    rentvbuywindow.exec();
}

void MainWindow::on_launchcompoundinterest_clicked() {
    CompoundInterestUI compoundinterestwindow;
    compoundinterestwindow.setModal(true);
    compoundinterestwindow.exec();
}

void MainWindow::on_launchpresentvalue_clicked() {
    PresentValueUI presentvaluewindow;
    presentvaluewindow.setModal(true);
    presentvaluewindow.exec();
}
