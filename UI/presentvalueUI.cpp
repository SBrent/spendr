#include "presentvalueUI.h"

PresentValueUI::PresentValueUI(QWidget *parent) :
    QDialog(parent) {
    SetupCalculator(); // Initializer function when dialog is created. Can set up all our widgets
}

PresentValueUI::~PresentValueUI() {
}

void PresentValueUI::SetupCalculator() {
    // Set window title
    QWidget::setWindowTitle ("Present Value Calculator");

    // Main layout
    QVBoxLayout* mainLayout = new QVBoxLayout;

    // Vert box for single column of labels & input fields
    QVBoxLayout* vertBox = new QVBoxLayout;

    // Boxes for all parameters
    QHBoxLayout* futureValueBox = new QHBoxLayout;
    QHBoxLayout* periodsBox = new QHBoxLayout;
    QHBoxLayout* interestRateBox = new QHBoxLayout;

    // Box for calculate button
    QHBoxLayout* calcButtonBox = new QHBoxLayout;

    // Boxes for results
    QHBoxLayout* presentValueBox = new QHBoxLayout;

    // Labels for all user inputs & results
    QLabel* futureValueLabel = new QLabel("Future value");
    QLabel* periodsLabel = new QLabel("Number of periods");
    QLabel* interestRateLabel = new QLabel("Interest rate");
    QLabel* presentValueLabel = new QLabel("Present value");

    // Input fields for all user inputs
    QDoubleSpinBox* futureValueSpin = new QDoubleSpinBox();
    futureValueSpin->setObjectName("futureValueSpin");
    futureValueSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    futureValueSpin->setPrefix("$");
    futureValueSpin->setMinimum(0);
    futureValueSpin->setMaximum(10000000);
    futureValueSpin->setValue(100000);
    futureValueSpin->setMinimumWidth(100);

    QSpinBox* periodsSpin = new QSpinBox();
    periodsSpin->setObjectName("periodsSpin");
    periodsSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    periodsSpin->setSuffix(" years");
    periodsSpin->setMinimum(0);
    periodsSpin->setMaximum(60);
    periodsSpin->setValue(10);
    periodsSpin->setMinimumWidth(100);

    QDoubleSpinBox* interestRateSpin = new QDoubleSpinBox();
    interestRateSpin->setObjectName("interestRateSpin");
    interestRateSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    interestRateSpin->setSuffix("%");
    interestRateSpin->setMinimum(0);
    interestRateSpin->setMaximum(100);
    interestRateSpin->setValue(.04 * 100); // so displayed as percent vs. decimal
    interestRateSpin->setMinimumWidth(100);

    // Calculate button
    QPushButton* calculate = new QPushButton;
    calculate->setText("Calculate");
    calculate->setMaximumWidth(100);
    connect(calculate,SIGNAL(clicked(bool)),this,SLOT(Calculate()));  // tie calcuation function to click event of button

    // Results
    QDoubleSpinBox* presentValue = new QDoubleSpinBox();
    presentValue->setObjectName("presentValue");
    presentValue->setButtonSymbols(QAbstractSpinBox::NoButtons);
    presentValue->setPrefix("$");
    presentValue->setMinimum(0);
    presentValue->setMaximum(10000000);
    presentValue->setReadOnly(true);

    // Place label-input pairs (or buttons) in previously created horizontal layout boxes
    futureValueBox->addWidget(futureValueLabel);
    futureValueBox->addWidget(futureValueSpin);
    periodsBox->addWidget(periodsLabel);
    periodsBox->addWidget(periodsSpin);
    interestRateBox->addWidget(interestRateLabel);
    interestRateBox->addWidget(interestRateSpin);

    calcButtonBox->addWidget(calculate);

    presentValueBox->addWidget(presentValueLabel);
    presentValueBox->addWidget(presentValue);

    // Assign layout boxes to the vertical layout
    vertBox->addLayout(futureValueBox);
    vertBox->addLayout(interestRateBox);
    vertBox->addLayout(periodsBox);
    vertBox->addLayout(calcButtonBox);
    vertBox->addLayout(presentValueBox);
    vertBox->setAlignment(Qt::AlignTop);

    // Add vertical box of user entry boxes to the main layout
    mainLayout->addLayout(vertBox);

    // Apply entire layout to widget
    setLayout(mainLayout);
}

void PresentValueUI::Calculate() {


    auto futureValue = QObject::findChild<QDoubleSpinBox*>("futureValueSpin")->value();
    auto periods = QObject::findChild<QSpinBox*>("periodsSpin")->value();
    auto interestRate = QObject::findChild<QDoubleSpinBox*>("interestRateSpin")->value() / 100;

    auto presentValue = m_presentValueCalc.presentValueCalc(futureValue, periods, interestRate);

    // Display present value
    auto presentValueSpin = QObject::findChild<QDoubleSpinBox*>("presentValue");
    presentValueSpin->setValue(presentValue);
}
