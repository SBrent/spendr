#include "downpaymentUI.h"

DownPaymentCalculatorUI::DownPaymentCalculatorUI(QWidget *parent) :
    QDialog(parent) { 
    SetupCalculator();
}

DownPaymentCalculatorUI::~DownPaymentCalculatorUI() {
}


void DownPaymentCalculatorUI::SetupCalculator() {
    // Set window title
    QWidget::setWindowTitle ("Down Payment Calculator");

    // Main layout
    QVBoxLayout* mainLayout = new QVBoxLayout;

    // Vert box for single column of labels & input fields
    QVBoxLayout* vertBox = new QVBoxLayout;

    // Boxes for all parameters
    QHBoxLayout* homePriceBox = new QHBoxLayout;
    QHBoxLayout* downPaymentBox = new QHBoxLayout;

    // Box for calculate button
    QHBoxLayout* calcButtonBox = new QHBoxLayout;

    // Boxes for results
    QHBoxLayout* fivePercentResultsBox = new QHBoxLayout;
    QHBoxLayout* tenPercentResultsBox = new QHBoxLayout;
    QHBoxLayout* twentyPercentResultsBox = new QHBoxLayout;
    QHBoxLayout* customPercentResultsBox = new QHBoxLayout;

    // Labels for all user inputs & results
    QLabel* homePriceLabel = new QLabel("Home price");
    QLabel* downPaymentLabel = new QLabel("Custom down payment percentage");

    QLabel* fivePercentDownLabel = new QLabel("5% down payment");
    QLabel* tenPercentDownLabel = new QLabel("10% down payment");
    QLabel* twentyPercentDownLabel = new QLabel("20% down payment");
    QLabel* customPercentDownLabel = new QLabel("Custom down payment");

    // Input fields for all user inputs
    QSpinBox* homePriceSpin = new QSpinBox();
    homePriceSpin->setObjectName("homePriceSpin");        // This is useful if we want to find the widget in other functions and do things like grab the value out of it
    homePriceSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    homePriceSpin->setPrefix("$");
    homePriceSpin->setMinimum(0);
    homePriceSpin->setMaximum(2000000);
    homePriceSpin->setValue(m_downCalc.getM_purchasePrice());
    homePriceSpin->setMinimumWidth(100);

    QDoubleSpinBox* downPaymentSpin = new QDoubleSpinBox();
    downPaymentSpin->setObjectName("downPaymentSpin");
    downPaymentSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    downPaymentSpin->setSuffix("%");
    downPaymentSpin->setMinimum(0);
    downPaymentSpin->setMaximum(100);
    downPaymentSpin->setMinimumWidth(100);
    downPaymentSpin->clear();

    // Calculate button
    QPushButton* calculate = new QPushButton;
    calculate->setText("Calculate");
    calculate->setMaximumWidth(100);
    connect(calculate,SIGNAL(clicked(bool)),this,SLOT(Calculate()));  // tie calcuation function to click event of button

    // Display fields for all calculation results
    QDoubleSpinBox* fivePercentDownSpin = new QDoubleSpinBox;
    fivePercentDownSpin->setObjectName("fivePercentDownSpin");
    fivePercentDownSpin->setPrefix("$");
    fivePercentDownSpin->setMinimum(0);
    fivePercentDownSpin->setMaximum(10000000);
    fivePercentDownSpin->setDecimals(2);
    fivePercentDownSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    fivePercentDownSpin->setMinimumWidth(100);
    fivePercentDownSpin->setReadOnly(true);

    QDoubleSpinBox* tenPercentDownSpin = new QDoubleSpinBox;
    tenPercentDownSpin->setObjectName("tenPercentDownSpin");
    tenPercentDownSpin->setPrefix("$");
    tenPercentDownSpin->setMinimum(0);
    tenPercentDownSpin->setMaximum(10000000);
    tenPercentDownSpin->setDecimals(2);
    tenPercentDownSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    tenPercentDownSpin->setMinimumWidth(100);
    tenPercentDownSpin->setReadOnly(true);

    QDoubleSpinBox* twentyPercentDownSpin = new QDoubleSpinBox;
    twentyPercentDownSpin->setObjectName("twentyPercentDownSpin");
    twentyPercentDownSpin->setPrefix("$");
    twentyPercentDownSpin->setMinimum(0);
    twentyPercentDownSpin->setMaximum(10000000);
    twentyPercentDownSpin->setDecimals(2);
    twentyPercentDownSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    twentyPercentDownSpin->setMinimumWidth(100);
    twentyPercentDownSpin->setReadOnly(true);

    QDoubleSpinBox* customPercentDownSpin = new QDoubleSpinBox;
    customPercentDownSpin->setObjectName("customPercentDownSpin");
    customPercentDownSpin->setPrefix("$");
    customPercentDownSpin->setMinimum(0);
    customPercentDownSpin->setMaximum(10000000);
    customPercentDownSpin->setDecimals(2);
    customPercentDownSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    customPercentDownSpin->setMinimumWidth(100);
    customPercentDownSpin->setReadOnly(true);

    // Place label-input pairs (or buttons) in previously created horizontal layout boxes
    homePriceBox->addWidget(homePriceLabel);
    homePriceBox->addWidget(homePriceSpin);
    downPaymentBox->addWidget(downPaymentLabel);
    downPaymentBox->addWidget(downPaymentSpin);

    calcButtonBox->addWidget(calculate);

    fivePercentResultsBox->addWidget(fivePercentDownLabel);
    fivePercentResultsBox->addWidget(fivePercentDownSpin);
    tenPercentResultsBox->addWidget(tenPercentDownLabel);
    tenPercentResultsBox->addWidget(tenPercentDownSpin);
    twentyPercentResultsBox->addWidget(twentyPercentDownLabel);
    twentyPercentResultsBox->addWidget(twentyPercentDownSpin);
    customPercentResultsBox->addWidget(customPercentDownLabel);
    customPercentResultsBox->addWidget(customPercentDownSpin);

    // Assign layout boxes to the vertical layout
    vertBox->addLayout(homePriceBox);
    vertBox->addLayout(downPaymentBox);
    vertBox->addLayout(calcButtonBox);
    vertBox->addLayout(fivePercentResultsBox);
    vertBox->addLayout(tenPercentResultsBox);
    vertBox->addLayout(twentyPercentResultsBox);
    vertBox->addLayout(customPercentResultsBox);
    vertBox->setAlignment(Qt::AlignTop);

    // Add vertical box of user entry boxes to the main layout
    mainLayout->addLayout(vertBox);

    // Apply entire layout to widget
    setLayout(mainLayout);
}

void DownPaymentCalculatorUI::Calculate() {

    // Calculate downpayments using different percentages
    m_downCalc.setM_purchasePrice(QObject::findChild<QSpinBox*>("homePriceSpin")->value());

    m_downCalc.setM_downPaymentPercent(0.05);
    auto fiveDownAmt = m_downCalc.DownPayment();

    m_downCalc.setM_downPaymentPercent(0.10);
    auto tenDownAmt = m_downCalc.DownPayment();

    m_downCalc.setM_downPaymentPercent(0.20);
    auto twentyDownAmt = m_downCalc.DownPayment();

    auto customDownAmt = -1;
    if (QObject::findChild<QDoubleSpinBox*>("downPaymentSpin")->value()) {   // ensure field not empty
        m_downCalc.setM_downPaymentPercent(QObject::findChild<QDoubleSpinBox*>("downPaymentSpin")->value()/100);  // get custom percentage as decimal
        customDownAmt = m_downCalc.DownPayment();
    }

    // Display results
    QObject::findChild<QDoubleSpinBox*>("fivePercentDownSpin")->setValue(fiveDownAmt);
    QObject::findChild<QDoubleSpinBox*>("tenPercentDownSpin")->setValue(tenDownAmt);
    QObject::findChild<QDoubleSpinBox*>("twentyPercentDownSpin")->setValue(twentyDownAmt);
    if (customDownAmt == -1) {
        QObject::findChild<QDoubleSpinBox*>("customPercentDownSpin")->clear();   // if no custom percentage, display nothing
    }
    else {
        QObject::findChild<QDoubleSpinBox*>("customPercentDownSpin")->setValue(customDownAmt);
    }

}
