#include <iostream>
#include "amortizationUI.h"

AmortizationCalculatorUI::AmortizationCalculatorUI(QWidget *parent) :
    QDialog(parent) { 
    SetupCalculator();
}

AmortizationCalculatorUI::~AmortizationCalculatorUI() {
}


void AmortizationCalculatorUI::SetupCalculator() {
    // Set window title
    QWidget::setWindowTitle ("Amortization Calculator");

    // Main layout
    QVBoxLayout* mainLayout = new QVBoxLayout;

    // Vert box for single column of labels & input fields
    QVBoxLayout* vertBox = new QVBoxLayout;

    // Boxes for all parameters
    QHBoxLayout* loanAmountBox = new QHBoxLayout;
    QHBoxLayout* interestRateBox = new QHBoxLayout;
    QHBoxLayout* loanTermBox = new QHBoxLayout;

    // Box for calculate button
    QHBoxLayout* calcButtonBox = new QHBoxLayout;

    // Boxes for results
    QHBoxLayout* monthlyPaymentBox = new QHBoxLayout;
    QHBoxLayout* numOfPaymentsBox = new QHBoxLayout;
    QHBoxLayout* totalPaymentBox = new QHBoxLayout;
    QHBoxLayout* totalInterestBox = new QHBoxLayout;
    QHBoxLayout* amortSchedBox = new QHBoxLayout;
    QHBoxLayout* amortSchedTableBox = new QHBoxLayout;

    // Labels for all user inputs & results
    QLabel* loanAmountLabel = new QLabel("Loan amount");
    QLabel* interestRateLabel = new QLabel("Interest rate");
    QLabel* loanTermLabel = new QLabel("Loan Term");

    QLabel* monthlyPaymentLabel = new QLabel("Monthly payment");
    QLabel* numOfPaymentsLabel = new QLabel("Num of payments");
    QLabel* totalPaymentLabel = new QLabel("Total payment");
    QLabel* totalInterestLabel = new QLabel("Total interest");
    QLabel* amortSchedLabel = new QLabel("Monthly Amortization schedule");

    // Input fields for all user inputs
    QDoubleSpinBox* loanAmountSpin = new QDoubleSpinBox();
    loanAmountSpin->setObjectName("loanAmountSpin");
    loanAmountSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    loanAmountSpin->setPrefix("$");
    loanAmountSpin->setMinimum(0);
    loanAmountSpin->setMaximum(5000000);
    loanAmountSpin->setValue(m_amortCalc.getM_loanAmount());
    loanAmountSpin->setMinimumWidth(100);

    QDoubleSpinBox* interestRateSpin = new QDoubleSpinBox();
    interestRateSpin->setObjectName("interestRateSpin");
    interestRateSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    interestRateSpin->setSuffix("%");
    interestRateSpin->setMinimum(0);
    interestRateSpin->setMaximum(100);
    interestRateSpin->setValue(m_amortCalc.getM_interestRate()*100);   // so displayed as percent vs. decimal
    interestRateSpin->setMinimumWidth(100);

    QSpinBox* loanTermSpin = new QSpinBox();
    loanTermSpin->setObjectName("loanTermSpin");
    loanTermSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    loanTermSpin->setSuffix(" years");
    loanTermSpin->setMinimum(0);
    loanTermSpin->setMaximum(60);
    loanTermSpin->setValue(m_amortCalc.getM_loanTerm());
    loanTermSpin->setMinimumWidth(100);

    // Calculate button
    QPushButton* calculate = new QPushButton;
    calculate->setText("Calculate");
    calculate->setMaximumWidth(100);
    connect(calculate,SIGNAL(clicked(bool)),this,SLOT(Calculate()));  // tie calcuation function to click event of button

    // Display fields for all calculation results
    QDoubleSpinBox* monthlyPaymentSpin = new QDoubleSpinBox;
    monthlyPaymentSpin->setObjectName("monthlyPaymentSpin");
    monthlyPaymentSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    monthlyPaymentSpin->setPrefix("$");
    monthlyPaymentSpin->setMinimum(0);
    monthlyPaymentSpin->setMaximum(500000);
    monthlyPaymentSpin->setDecimals(2);
    monthlyPaymentSpin->setMinimumWidth(100);
    monthlyPaymentSpin->setReadOnly(true);

    QSpinBox* numOfPaymentsSpin = new QSpinBox;
    numOfPaymentsSpin->setObjectName("numOfPaymentsSpin");
    numOfPaymentsSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    numOfPaymentsSpin->setMinimum(0);
    numOfPaymentsSpin->setMaximum(10000);
    numOfPaymentsSpin->setMinimumWidth(100);
    numOfPaymentsSpin->setReadOnly(true);

    QDoubleSpinBox* totalPaymentSpin = new QDoubleSpinBox;
    totalPaymentSpin->setObjectName("totalPaymentSpin");
    totalPaymentSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    totalPaymentSpin->setPrefix("$");
    totalPaymentSpin->setMinimum(0);
    totalPaymentSpin->setMaximum(10000000);
    totalPaymentSpin->setDecimals(2);
    totalPaymentSpin->setMinimumWidth(100);
    totalPaymentSpin->setReadOnly(true);

    QDoubleSpinBox* totalInterestSpin = new QDoubleSpinBox;
    totalInterestSpin->setObjectName("totalInterestSpin");
    totalInterestSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    totalInterestSpin->setPrefix("$");
    totalInterestSpin->setMinimum(0);
    totalInterestSpin->setMaximum(10000000);
    totalInterestSpin->setDecimals(2);
    totalInterestSpin->setMinimumWidth(100);
    totalInterestSpin->setReadOnly(true);

    QTableWidget* amortTable = new QTableWidget(this);
    amortTable->setObjectName("amortTable");
    amortTable->setColumnCount(4);
    amortTable->setMinimumWidth(475);
    amortTable->setMinimumHeight(300);
    amortTable->setHorizontalHeaderItem(0, new QTableWidgetItem("Beg Balance"));
    amortTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Interest Amt"));
    amortTable->setHorizontalHeaderItem(2, new QTableWidgetItem("Principal Amt"));
    amortTable->setHorizontalHeaderItem(3, new QTableWidgetItem("End Balance"));
    amortTable->hide();  // hide until calculate button pushed

    // Place label-input pairs (or buttons) in previously created horizontal layout boxes 
    loanAmountBox->addWidget(loanAmountLabel);
    loanAmountBox->addWidget(loanAmountSpin);
    interestRateBox->addWidget(interestRateLabel);
    interestRateBox->addWidget(interestRateSpin);
    loanTermBox->addWidget(loanTermLabel);
    loanTermBox->addWidget(loanTermSpin);

    calcButtonBox->addWidget(calculate);

    monthlyPaymentBox->addWidget(monthlyPaymentLabel);
    monthlyPaymentBox->addWidget(monthlyPaymentSpin);
    numOfPaymentsBox->addWidget(numOfPaymentsLabel);
    numOfPaymentsBox->addWidget(numOfPaymentsSpin);
    totalPaymentBox->addWidget(totalPaymentLabel);
    totalPaymentBox->addWidget(totalPaymentSpin);
    totalInterestBox->addWidget(totalInterestLabel);
    totalInterestBox->addWidget(totalInterestSpin);
    amortSchedBox->addWidget(amortSchedLabel);
    amortSchedTableBox->addWidget(amortTable);

    // Assign layout boxes to the vertical layout
    vertBox->addLayout(loanAmountBox);
    vertBox->addLayout(interestRateBox);
    vertBox->addLayout(loanTermBox);
    vertBox->addLayout(calcButtonBox);
    vertBox->addLayout(monthlyPaymentBox);
    vertBox->addLayout(numOfPaymentsBox);
    vertBox->addLayout(totalPaymentBox);
    vertBox->addLayout(totalInterestBox);
    vertBox->addLayout(amortSchedBox);
    vertBox->addLayout(amortSchedTableBox);
    vertBox->setAlignment(Qt::AlignTop);

    // Add vertical box of user entry boxes to the main layout
    mainLayout->addLayout(vertBox);

    // Apply entire layout to widget
    setLayout(mainLayout);
}

void AmortizationCalculatorUI::Calculate() {

    // Set values per user inputs
    SetInputValues();

    // Calculate outputs & create amort schedule
    double monthlyPayment = m_amortCalc.AmortizationCalc();
    int numOfPayments = m_amortCalc.getM_loanTerm() * 12;
    double totalOfPayments = monthlyPayment * numOfPayments;
    double totalInterest = totalOfPayments - m_amortCalc.getM_loanAmount();

    // Display single val results
    QObject::findChild<QDoubleSpinBox*>("monthlyPaymentSpin")->setValue(monthlyPayment);
    QObject::findChild<QSpinBox*>("numOfPaymentsSpin")->setValue(numOfPayments);
    QObject::findChild<QDoubleSpinBox*>("totalPaymentSpin")->setValue(totalOfPayments);
    QObject::findChild<QDoubleSpinBox*>("totalInterestSpin")->setValue(totalInterest);

    // Display monthly amortization schedule
    auto amortTable = QObject::findChild<QTableWidget*>("amortTable");
    amortTable->show();
    amortTable->setRowCount(m_amortCalc.getM_loanTerm() * 12);
    for(int count = 0; count < numOfPayments; count++) {
        int col = 0;
        QTableWidgetItem *newItem1 = new QTableWidgetItem;
        QTableWidgetItem *newItem2 = new QTableWidgetItem;
        QTableWidgetItem *newItem3 = new QTableWidgetItem;
        QTableWidgetItem *newItem4 = new QTableWidgetItem;
        newItem1->setData(Qt::EditRole, ((float)((int)(m_amortCalc.getM_beginningBalanceAmounts()[count]
                                                       * 100 + .5))) / 100);   // round to 2 decimals
        newItem2->setData(Qt::EditRole, ((float)((int)(m_amortCalc.getM_interestAmounts()[count]
                                                       * 100 + .5))) / 100);
        newItem3->setData(Qt::EditRole, ((float)((int)(m_amortCalc.getM_principalAmounts()[count]
                                                       * 100 + .5))) / 100);
        newItem4->setData(Qt::EditRole, ((float)((int)(m_amortCalc.getM_endingBalanceAmounts()[count]
                                                       * 100 + .5))) / 100);
        amortTable->setItem(count, col, newItem1);
        amortTable->setItem(count, col+1, newItem2);
        amortTable->setItem(count, col+2, newItem3);
        amortTable->setItem(count, col+3, newItem4);
    }
}

void AmortizationCalculatorUI::SetInputValues() {
    // Set values per user inputs
    m_amortCalc.setM_loanAmount(QObject::findChild<QDoubleSpinBox*>("loanAmountSpin")->value());
    m_amortCalc.setM_interestRate(QObject::findChild<QDoubleSpinBox*>("interestRateSpin")->value()/100);
    m_amortCalc.setM_loanTerm(QObject::findChild<QSpinBox*>("loanTermSpin")->value());
}
