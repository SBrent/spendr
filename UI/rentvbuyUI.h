#ifndef RENTVBUYUI_H
#define RENTVBUYUI_H

#include "Calculators/RentVsBuyCalculator/rentvbuycalculator.h"

#include <QDialog>
#include <QLineEdit>
#include <QtWidgets>

namespace Ui {
class RentVBuyCalculatorUI;
}

class RentVBuyCalculatorUI : public QDialog
{
    Q_OBJECT

public:
    explicit RentVBuyCalculatorUI(QWidget *parent = nullptr);
    ~RentVBuyCalculatorUI();

private slots:
    void Calculate();

private:
    RentVBuyCalculator m_rvbCalc;
    void SetupCalculator();
    void SetInputValues();
};

#endif // RENTVBUYUI_H
