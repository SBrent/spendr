#ifndef PRESENTVALUEUI_H
#define PRESENTVALUEUI_H

#include "Calculators/PresentValueCalculator/presentvaluecalculator.h"

#include <QDialog>
#include <QLineEdit>
#include <QtWidgets>

namespace Ui {
class PresentValueUI;
}

class PresentValueUI : public QDialog {
    Q_OBJECT

public:
    explicit PresentValueUI(QWidget *parent = nullptr);
    ~PresentValueUI();

private slots:
    void Calculate();
    //void SetInputValues();

private:
    PresentValueCalculator m_presentValueCalc;
    void SetupCalculator();
};

#endif // PRESENTVALUEUI_H
