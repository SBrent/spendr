#include "rentvbuyUI.h"

RentVBuyCalculatorUI::RentVBuyCalculatorUI(QWidget *parent) :
    QDialog(parent) {
    SetupCalculator(); // Initializer function when dialog is created. Can set up all our widgets
}

RentVBuyCalculatorUI::~RentVBuyCalculatorUI() {
}


void RentVBuyCalculatorUI::SetupCalculator() {
    // Set window title
    QWidget::setWindowTitle ("Rent vs. Buy Calculator");

    // These layouts can be laid into each other.
    // Can designate one as a "main" to contain everything, set a fixed size, etc.
    QVBoxLayout* mainLayout = new QVBoxLayout;

    // We'll use these to contain our buttons, fields, labels, et al
    QVBoxLayout* vertBox = new QVBoxLayout;

    // Boxes for all parameters
    QHBoxLayout* homePriceBox = new QHBoxLayout;
    QHBoxLayout* downPaymentBox = new QHBoxLayout;
    QHBoxLayout* loanTermBox = new QHBoxLayout;
    QHBoxLayout* mortgageRateBox = new QHBoxLayout;
    QHBoxLayout* closingCostsBox = new QHBoxLayout;
    QHBoxLayout* propertyTaxRateBox = new QHBoxLayout;
    QHBoxLayout* insuranceCostBox = new QHBoxLayout;
    QHBoxLayout* maintenanceCostBox = new QHBoxLayout;
    QHBoxLayout* appreciationRateBox = new QHBoxLayout;
    QHBoxLayout* saleClosingCostsBox = new QHBoxLayout;
    QHBoxLayout* monthlyRentBox = new QHBoxLayout;
    QHBoxLayout* securityDepositBox = new QHBoxLayout;
    QHBoxLayout* rentIncreaseRateBox = new QHBoxLayout;
    QHBoxLayout* investmentRateBox = new QHBoxLayout;
    QHBoxLayout* homeYearsBox = new QHBoxLayout;

    // Box for calculate button
    QHBoxLayout* calcButtonBox = new QHBoxLayout;

    // Boxes for results
    QHBoxLayout* buyResultsBox = new QHBoxLayout;
    QHBoxLayout* rentResultsBox = new QHBoxLayout;
    QHBoxLayout* buyOrRentResultBox = new QHBoxLayout;

    // Labels for all user inputs & results
    QLabel* homePriceLabel = new QLabel("Home price");
    QLabel* downPaymentLabel = new QLabel("Down payment");
    QLabel* loanTermLabel = new QLabel("Loan term");
    QLabel* mortgageRateLabel = new QLabel("Mortgage rate");
    QLabel* closingCostsLabel = new QLabel("Buying closing costs");
    QLabel* propertyTaxRateLabel = new QLabel("Property tax rate");
    QLabel* insuranceCostLabel = new QLabel("Home insurance cost/year");
    QLabel* maintenanceCostLabel = new QLabel("Maintenance cost/year");
    QLabel* appreciationRateLabel = new QLabel("Appreciation rate");
    QLabel* saleClosingCostsLabel = new QLabel("Selling closing costs");
    QLabel* monthlyRentLabel = new QLabel("Monthly rent");
    QLabel* securityDepositLabel = new QLabel("Security deposit");
    QLabel* rentIncreaseRateLabel = new QLabel("Rental increase rate");
    QLabel* investmentRateLabel = new QLabel("Investment earnings rate");
    QLabel* homeYearsLabel = new QLabel("Years in home");

    QLabel* totalBuyCostLabel = new QLabel("Total Buying Costs");
    QLabel* avgMonthlyBuyCostLabel = new QLabel(" Average");
    QLabel* totalRentCostLabel = new QLabel("Total Renting Costs");
    QLabel* avgMonthlyRentCostLabel = new QLabel(" Average");
    QLabel* buyOrRentLabel = new QLabel("Less expensive option");

    // Input fields for all user inputs
    // TODO: Initialize with calculator defaults
    QSpinBox* homePriceSpin = new QSpinBox();
    homePriceSpin->setObjectName("homePriceSpin");        // This is useful if we want to find the widget in other functions and do things like grab the value out of it
    homePriceSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    homePriceSpin->setPrefix("$");
    homePriceSpin->setMinimum(0);
    homePriceSpin->setMaximum(2000000);
    homePriceSpin->setValue(m_rvbCalc.getM_purchasePrice());
    homePriceSpin->setMinimumWidth(100);

    QDoubleSpinBox* downPaymentSpin = new QDoubleSpinBox();
    downPaymentSpin->setObjectName("downPaymentSpin");
    downPaymentSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    downPaymentSpin->setSuffix("%");
    downPaymentSpin->setMinimum(0);
    downPaymentSpin->setMaximum(100);
    downPaymentSpin->setValue(m_rvbCalc.getM_downPaymentPercent()*100);
    downPaymentSpin->setMinimumWidth(100);

    QSpinBox* loanTermSpin = new QSpinBox();
    loanTermSpin->setObjectName("loanTermSpin");
    loanTermSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    loanTermSpin->setSuffix(" years");
    loanTermSpin->setMinimum(5);
    loanTermSpin->setMaximum(30);
    loanTermSpin->setValue(m_rvbCalc.getM_loanTerm());
    loanTermSpin->setMinimumWidth(100);

    QDoubleSpinBox* mortgageRateSpin = new QDoubleSpinBox();
    mortgageRateSpin->setObjectName("mortgageRateSpin");
    mortgageRateSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    mortgageRateSpin->setSuffix("%");
    mortgageRateSpin->setMinimum(0);
    mortgageRateSpin->setMaximum(20);
    mortgageRateSpin->setValue(m_rvbCalc.getM_mortgageRate()*100);
    mortgageRateSpin->setMinimumWidth(100);

    QDoubleSpinBox* closingCostsSpin = new QDoubleSpinBox();
    closingCostsSpin->setObjectName("closingCostsSpin");
    closingCostsSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    closingCostsSpin->setSuffix("%");
    closingCostsSpin->setMinimum(0);
    closingCostsSpin->setMaximum(100);
    closingCostsSpin->setValue(m_rvbCalc.getM_buyingClosingCost()*100);
    closingCostsSpin->setMinimumWidth(100);

    QDoubleSpinBox* propertyTaxRateSpin = new QDoubleSpinBox();
    propertyTaxRateSpin->setObjectName("propertyTaxRateSpin");
    propertyTaxRateSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    propertyTaxRateSpin->setSuffix("%");
    propertyTaxRateSpin->setMinimum(0);
    propertyTaxRateSpin->setMaximum(100);
    propertyTaxRateSpin->setValue(m_rvbCalc.getM_propertyTaxRate()*100);
    propertyTaxRateSpin->setMinimumWidth(100);

    QSpinBox* insuranceCostSpin = new QSpinBox();
    insuranceCostSpin->setObjectName("insuranceCostSpin");
    insuranceCostSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    insuranceCostSpin->setPrefix("$");
    insuranceCostSpin->setMinimum(0);
    insuranceCostSpin->setMaximum(10000);
    insuranceCostSpin->setValue(m_rvbCalc.getM_annualInsurance());
    insuranceCostSpin->setMinimumWidth(100);

    QSpinBox* maintenanceCostSpin = new QSpinBox();
    maintenanceCostSpin->setObjectName("maintenaceCostSpin");
    maintenanceCostSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    maintenanceCostSpin->setPrefix("$");
    maintenanceCostSpin->setMinimum(0);
    maintenanceCostSpin->setMaximum(10000);
    maintenanceCostSpin->setValue(m_rvbCalc.getM_annualMaintenance());
    maintenanceCostSpin->setMinimumWidth(100);

    QDoubleSpinBox* appreciationRateSpin = new QDoubleSpinBox();
    appreciationRateSpin->setObjectName("appreciationRateSpin");
    appreciationRateSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    appreciationRateSpin->setSuffix("%");
    appreciationRateSpin->setMinimum(0);
    appreciationRateSpin->setMaximum(100);
    appreciationRateSpin->setValue(m_rvbCalc.getM_homeAppreciationRate()*100);
    appreciationRateSpin->setMinimumWidth(100);

    QDoubleSpinBox* saleClosingCostsSpin = new QDoubleSpinBox();
    saleClosingCostsSpin->setObjectName("saleClosingCostsSpin");
    saleClosingCostsSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    saleClosingCostsSpin->setSuffix("%");
    saleClosingCostsSpin->setMinimum(0);
    saleClosingCostsSpin->setMaximum(100);
    saleClosingCostsSpin->setValue(m_rvbCalc.getM_sellingClosingCost()*100);
    saleClosingCostsSpin->setMinimumWidth(100);

    QSpinBox* monthlyRentSpin = new QSpinBox();
    monthlyRentSpin->setObjectName("monthlyRentSpin");
    monthlyRentSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    monthlyRentSpin->setPrefix("$");
    monthlyRentSpin->setMinimum(0);
    monthlyRentSpin->setMaximum(10000);
    monthlyRentSpin->setValue(m_rvbCalc.getM_monthlyRent());
    monthlyRentSpin->setMinimumWidth(100);

    QSpinBox* securityDepositSpin = new QSpinBox();
    securityDepositSpin->setObjectName("securityDepositSpin");
    securityDepositSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    securityDepositSpin->setPrefix("$");
    securityDepositSpin->setMinimum(0);
    securityDepositSpin->setMaximum(10000);
    securityDepositSpin->setValue(m_rvbCalc.getM_securityDeposit());
    securityDepositSpin->setMinimumWidth(100);

    QDoubleSpinBox* rentIncreaseRateSpin = new QDoubleSpinBox();
    rentIncreaseRateSpin->setObjectName("rentIncreaseRateSpin");
    rentIncreaseRateSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    rentIncreaseRateSpin->setSuffix("%");
    rentIncreaseRateSpin->setMinimum(0);
    rentIncreaseRateSpin->setMaximum(100);
    rentIncreaseRateSpin->setValue(m_rvbCalc.getM_rentalIncreaseRate()*100);
    rentIncreaseRateSpin->setMinimumWidth(100);

    QDoubleSpinBox* investmentRateSpin = new QDoubleSpinBox();
    investmentRateSpin->setObjectName("investmentRateSpin");
    investmentRateSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    investmentRateSpin->setSuffix("%");
    investmentRateSpin->setMinimum(0);
    investmentRateSpin->setMaximum(100);
    investmentRateSpin->setValue(m_rvbCalc.getM_investmentRate()*100);
    investmentRateSpin->setMinimumWidth(100);

    QSpinBox* homeYearsSpin = new QSpinBox();
    homeYearsSpin->setObjectName("homeYearsSpin");
    homeYearsSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    homeYearsSpin->setMinimum(0);
    homeYearsSpin->setMaximum(50);
    homeYearsSpin->setValue(m_rvbCalc.getM_nYears());
    homeYearsSpin->setMinimumWidth(100);

    // Calculate button
    QPushButton* calculate = new QPushButton;
    calculate->setText("Calculate");
    calculate->setMaximumWidth(100);
    connect(calculate,SIGNAL(clicked(bool)),this,SLOT(Calculate()));  // tie calcuation function to click event of button

    // Display fields for all calculation results
    QDoubleSpinBox* totalBuyCostSpin = new QDoubleSpinBox;
    totalBuyCostSpin->setObjectName("totalBuyCostsSpin");
    totalBuyCostSpin->setPrefix("$");
    totalBuyCostSpin->setMinimum(0);
    totalBuyCostSpin->setMaximum(10000000);
    totalBuyCostSpin->setDecimals(2);
    totalBuyCostSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    totalBuyCostSpin->setMinimumWidth(100);
    totalBuyCostSpin->setReadOnly(true);

    QDoubleSpinBox* avgMonthlyBuyCostSpin = new QDoubleSpinBox;
    avgMonthlyBuyCostSpin->setObjectName("avgMonthlyBuyCostSpin");
    avgMonthlyBuyCostSpin->setPrefix("$");
    avgMonthlyBuyCostSpin->setSuffix("/mo");
    avgMonthlyBuyCostSpin->setMinimum(0);
    avgMonthlyBuyCostSpin->setMaximum(50000);
    avgMonthlyBuyCostSpin->setDecimals(2);
    avgMonthlyBuyCostSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    avgMonthlyBuyCostSpin->setMinimumWidth(100);
    avgMonthlyBuyCostSpin->setReadOnly(true);

    QDoubleSpinBox* totalRentCostSpin = new QDoubleSpinBox;
    totalRentCostSpin->setObjectName("totalRentCostsSpin");
    totalRentCostSpin->setPrefix("$");
    totalRentCostSpin->setMinimum(0);
    totalRentCostSpin->setMaximum(10000000);
    totalRentCostSpin->setDecimals(2);
    totalRentCostSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    totalRentCostSpin->setMinimumWidth(100);
    totalRentCostSpin->setReadOnly(true);

    QDoubleSpinBox* avgMonthlyRentCostSpin = new QDoubleSpinBox;
    avgMonthlyRentCostSpin->setObjectName("avgMonthlyRentCostSpin");
    avgMonthlyRentCostSpin->setPrefix("$");
    avgMonthlyRentCostSpin->setSuffix("/mo");
    avgMonthlyRentCostSpin->setMinimum(0);
    avgMonthlyRentCostSpin->setMaximum(50000);
    avgMonthlyRentCostSpin->setDecimals(2);
    avgMonthlyRentCostSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    avgMonthlyRentCostSpin->setMinimumWidth(100);
    avgMonthlyRentCostSpin->setReadOnly(true);

    QLineEdit* buyOrRentLineEdit = new QLineEdit;
    buyOrRentLineEdit->setObjectName("buyOrRentLineEdit");
    buyOrRentLineEdit->setReadOnly(true);

    // Widgets added in order horizontally. Can also adjust spacing, alignment and such
    // Add the entry boxes with their labels
    homePriceBox->addWidget(homePriceLabel);
    homePriceBox->addWidget(homePriceSpin);

    downPaymentBox->addWidget(downPaymentLabel);
    downPaymentBox->addWidget(downPaymentSpin);

    loanTermBox->addWidget(loanTermLabel);
    loanTermBox->addWidget(loanTermSpin);

    mortgageRateBox->addWidget(mortgageRateLabel);
    mortgageRateBox->addWidget(mortgageRateSpin);

    closingCostsBox->addWidget(closingCostsLabel);
    closingCostsBox->addWidget(closingCostsSpin);

    propertyTaxRateBox->addWidget(propertyTaxRateLabel);
    propertyTaxRateBox->addWidget(propertyTaxRateSpin);

    insuranceCostBox->addWidget(insuranceCostLabel);
    insuranceCostBox->addWidget(insuranceCostSpin);

    maintenanceCostBox->addWidget(maintenanceCostLabel);
    maintenanceCostBox->addWidget(maintenanceCostSpin);

    appreciationRateBox->addWidget(appreciationRateLabel);
    appreciationRateBox->addWidget(appreciationRateSpin);

    saleClosingCostsBox->addWidget(saleClosingCostsLabel);
    saleClosingCostsBox->addWidget(saleClosingCostsSpin);

    monthlyRentBox->addWidget(monthlyRentLabel);
    monthlyRentBox->addWidget(monthlyRentSpin);

    securityDepositBox->addWidget(securityDepositLabel);
    securityDepositBox->addWidget(securityDepositSpin);

    rentIncreaseRateBox->addWidget(rentIncreaseRateLabel);
    rentIncreaseRateBox->addWidget(rentIncreaseRateSpin);

    investmentRateBox->addWidget(investmentRateLabel);
    investmentRateBox->addWidget(investmentRateSpin);

    homeYearsBox->addWidget(homeYearsLabel);
    homeYearsBox->addWidget(homeYearsSpin);

    calcButtonBox->addWidget(calculate);

    buyResultsBox->addWidget(totalBuyCostLabel);
    buyResultsBox->addWidget(totalBuyCostSpin);
    buyResultsBox->addWidget(avgMonthlyBuyCostLabel);
    buyResultsBox->addWidget(avgMonthlyBuyCostSpin);

    rentResultsBox->addWidget(totalRentCostLabel);
    rentResultsBox->addWidget(totalRentCostSpin);
    rentResultsBox->addWidget(avgMonthlyRentCostLabel);
    rentResultsBox->addWidget(avgMonthlyRentCostSpin);

    buyOrRentResultBox->addWidget(buyOrRentLabel);
    buyOrRentResultBox->addWidget(buyOrRentLineEdit);

    // Assign entry boxes to the vertical layout. We can change this to a grid, etc.
    vertBox->addLayout(homePriceBox);
    vertBox->addLayout(downPaymentBox);
    vertBox->addLayout(loanTermBox);
    vertBox->addLayout(mortgageRateBox);
    vertBox->addLayout(closingCostsBox);
    vertBox->addLayout(propertyTaxRateBox);
    vertBox->addLayout(insuranceCostBox);
    vertBox->addLayout(maintenanceCostBox);
    vertBox->addLayout(appreciationRateBox);
    vertBox->addLayout(saleClosingCostsBox);
    vertBox->addLayout(monthlyRentBox);
    vertBox->addLayout(securityDepositBox);
    vertBox->addLayout(rentIncreaseRateBox);
    vertBox->addLayout(investmentRateBox);
    vertBox->addLayout(homeYearsBox);
    vertBox->addLayout(calcButtonBox);
    vertBox->addLayout(buyResultsBox);
    vertBox->addLayout(rentResultsBox);
    vertBox->addLayout(buyOrRentResultBox);
    vertBox->setAlignment(Qt::AlignTop);

    // Add vertical box of user entry boxes to the main layout
    mainLayout->addLayout(vertBox);
    setLayout(mainLayout); // This will apply the entire layout to the widget
}

void RentVBuyCalculatorUI::Calculate() {

    // Set member variables using input values
    SetInputValues();

    // Calculate & display costs and rent vs. buy decision
    auto buyCost = m_rvbCalc.TotalBuyingCostsAfterN();
    auto rentCost = m_rvbCalc.TotalRentingCostsAfterN();
    QObject::findChild<QDoubleSpinBox*>("totalBuyCostsSpin")->setValue(buyCost);
    QObject::findChild<QDoubleSpinBox*>("avgMonthlyBuyCostSpin")->setValue(buyCost/(12 * m_rvbCalc.getM_nYears()));
    QObject::findChild<QDoubleSpinBox*>("totalRentCostsSpin")->setValue(rentCost);
    QObject::findChild<QDoubleSpinBox*>("avgMonthlyRentCostSpin")->setValue(rentCost/(12 * m_rvbCalc.getM_nYears()));
    QObject::findChild<QLineEdit*>("buyOrRentLineEdit")->setText(((buyCost < rentCost) ? "BUY" : "RENT"));
}

void RentVBuyCalculatorUI::SetInputValues() {
    // Set member variable values using input values, converting percentages to decimal form
    m_rvbCalc.setM_purchasePrice(QObject::findChild<QSpinBox*>("homePriceSpin")->value());
    m_rvbCalc.setM_downPaymentPercent(QObject::findChild<QDoubleSpinBox*>("downPaymentSpin")->value()/100);
    m_rvbCalc.setM_loanTerm(QObject::findChild<QSpinBox*>("loanTermSpin")->value());
    m_rvbCalc.setM_mortgageRate(QObject::findChild<QDoubleSpinBox*>("mortgageRateSpin")->value()/100);
    m_rvbCalc.setM_buyingClosingCost(QObject::findChild<QDoubleSpinBox*>("closingCostsSpin")->value()/100);
    m_rvbCalc.setM_propertyTaxRate(QObject::findChild<QDoubleSpinBox*>("propertyTaxRateSpin")->value()/100);
    m_rvbCalc.setM_annualInsurance(QObject::findChild<QSpinBox*>("insuranceCostSpin")->value());
    m_rvbCalc.setM_annualMaintenance(QObject::findChild<QSpinBox*>("maintenaceCostSpin")->value());
    m_rvbCalc.setM_sellingClosingCost(QObject::findChild<QDoubleSpinBox*>("saleClosingCostsSpin")->value()/100);
    m_rvbCalc.setM_homeAppreciationRate(QObject::findChild<QDoubleSpinBox*>("appreciationRateSpin")->value()/100);
    m_rvbCalc.setM_monthlyRent(QObject::findChild<QSpinBox*>("monthlyRentSpin")->value());
    m_rvbCalc.setM_securityDeposit(QObject::findChild<QSpinBox*>("securityDepositSpin")->value());
    m_rvbCalc.setM_rentalIncreaseRate(QObject::findChild<QDoubleSpinBox*>("rentIncreaseRateSpin")->value()/100);
    m_rvbCalc.setM_investmentRate(QObject::findChild<QDoubleSpinBox*>("investmentRateSpin")->value()/100);
    m_rvbCalc.setM_nYears(QObject::findChild<QSpinBox*>("homeYearsSpin")->value());
}
