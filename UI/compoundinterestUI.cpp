#include "compoundinterestUI.h"

CompoundInterestUI::CompoundInterestUI(QWidget *parent) :
    QDialog(parent) {
    SetupCalculator(); // Initializer function when dialog is created. Can set up all our widgets
}

CompoundInterestUI::~CompoundInterestUI() {
}

void CompoundInterestUI::SetupCalculator() {
    // Set window title
    QWidget::setWindowTitle ("Compound Interest Calculator");

    // Main layout
    QVBoxLayout* mainLayout = new QVBoxLayout;

    // Vert box for single column of labels & input fields
    QVBoxLayout* vertBox = new QVBoxLayout;

    // Boxes for all parameters
    QHBoxLayout* principalBox = new QHBoxLayout;
    QHBoxLayout* interestRateBox = new QHBoxLayout;
    QHBoxLayout* yearsInvestedBox = new QHBoxLayout;
    QHBoxLayout* frequencyBox = new QHBoxLayout;

    // Box for calculate button
    QHBoxLayout* calcButtonBox = new QHBoxLayout;

    // Boxes for results
    QHBoxLayout* compoundInterestBox = new QHBoxLayout;
    QHBoxLayout* compoundInterestTableBox = new QHBoxLayout;

    // Labels for all user inputs & results
    QLabel* principalLabel = new QLabel("Principal");
    QLabel* interestRateLabel = new QLabel("Interest rate");
    QLabel* yearsInvestedLabel = new QLabel("Years invested");
    QLabel* frequencyLabel = new QLabel("Frequency");
    //QLabel* compoundInterestLabel = new QLabel("Compound interest");

    // Input fields for all user inputs
    QDoubleSpinBox* principalSpin = new QDoubleSpinBox();
    principalSpin->setObjectName("principalSpin");
    principalSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    principalSpin->setPrefix("$");
    principalSpin->setMinimum(0);
    principalSpin->setMaximum(10000000);
    principalSpin->setValue(100000);
    principalSpin->setMinimumWidth(100);

    QDoubleSpinBox* interestRateSpin = new QDoubleSpinBox();
    interestRateSpin->setObjectName("interestRateSpin");
    interestRateSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    interestRateSpin->setSuffix("%");
    interestRateSpin->setMinimum(0);
    interestRateSpin->setMaximum(100);
    interestRateSpin->setValue(.04 * 100); // so displayed as percent vs. decimal
    interestRateSpin->setMinimumWidth(100);

    QSpinBox* yearsInvestedSpin = new QSpinBox();
    yearsInvestedSpin->setObjectName("yearsInvestedSpin");
    yearsInvestedSpin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    yearsInvestedSpin->setSuffix(" years");
    yearsInvestedSpin->setMinimum(0);
    yearsInvestedSpin->setMaximum(60);
    yearsInvestedSpin->setValue(10);
    yearsInvestedSpin->setMinimumWidth(100);

    QComboBox* frequencyCombo = new QComboBox();
    frequencyCombo->setObjectName("frequencyCombo");
    frequencyCombo->addItem("Annual", QVariant(1));
    frequencyCombo->addItem("Semi-annual", QVariant(2));
    frequencyCombo->addItem("Monthly", QVariant(12));
    frequencyCombo->addItem("Daily", QVariant(365));
    frequencyCombo->setMinimumWidth(100);

    // Calculate button
    QPushButton* calculate = new QPushButton;
    calculate->setText("Calculate");
    calculate->setMaximumWidth(100);
    connect(calculate,SIGNAL(clicked(bool)),this,SLOT(Calculate()));  // tie calcuation function to click event of button


    QTableWidget* compoundInterestTable = new QTableWidget(this);
    compoundInterestTable->setObjectName("compoundInterestTable");
    compoundInterestTable->setColumnCount(2);
    compoundInterestTable->setMinimumWidth(525);
    compoundInterestTable->setMinimumHeight(400);
    compoundInterestTable->setHorizontalHeaderItem(0, new QTableWidgetItem("Year"));
    compoundInterestTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Future Value"));
    compoundInterestTable->hide();  // hide until calculate button pushed*/

    // Place label-input pairs (or buttons) in previously created horizontal layout boxes
    principalBox->addWidget(principalLabel);
    principalBox->addWidget(principalSpin);
    interestRateBox->addWidget(interestRateLabel);
    interestRateBox->addWidget(interestRateSpin);
    yearsInvestedBox->addWidget(yearsInvestedLabel);
    yearsInvestedBox->addWidget(yearsInvestedSpin);
    frequencyBox->addWidget(frequencyLabel);
    frequencyBox->addWidget(frequencyCombo);

    calcButtonBox->addWidget(calculate);

    //compoundInterestBox->addWidget(compoundInterestLabel);
    compoundInterestTableBox->addWidget(compoundInterestTable);

    // Assign layout boxes to the vertical layout
    vertBox->addLayout(principalBox);
    vertBox->addLayout(interestRateBox);
    vertBox->addLayout(yearsInvestedBox);
    vertBox->addLayout(frequencyBox);
    vertBox->addLayout(calcButtonBox);
    vertBox->addLayout(compoundInterestBox);
    vertBox->addLayout(compoundInterestTableBox);
    vertBox->setAlignment(Qt::AlignTop);

    // Add vertical box of user entry boxes to the main layout
    mainLayout->addLayout(vertBox);

    // Apply entire layout to widget
    setLayout(mainLayout);
}

void CompoundInterestUI::Calculate() {


    auto principal = QObject::findChild<QDoubleSpinBox*>("principalSpin")->value();
    auto interestRate = QObject::findChild<QDoubleSpinBox*>("interestRateSpin")->value() / 100;
    auto years = QObject::findChild<QSpinBox*>("yearsInvestedSpin")->value();
    auto frequency = QObject::findChild<QComboBox*>("frequencyCombo")->currentData().toInt();

    auto futureValues = m_compInterestCalc.compoundInterestCalc(principal, interestRate, years, frequency);


    // Display future values
    auto compoundInterestTable = QObject::findChild<QTableWidget*>("compoundInterestTable");
    compoundInterestTable->show();
    compoundInterestTable->setRowCount(years);
    for(size_t count = 0; count < futureValues.size(); count++) {
        QTableWidgetItem* year = new QTableWidgetItem;
        QTableWidgetItem* compoundInterest = new QTableWidgetItem;
        year->setData(Qt::EditRole, count);
        compoundInterest->setData(Qt::EditRole, futureValues[count]);

        compoundInterestTable->setItem(count, 0, year);
        compoundInterestTable->setItem(count, 1, compoundInterest);
    }
}
