#ifndef AMORTIZATIONUI_H
#define AMORTIZATIONUI_H

#include "Calculators/AmortizationCalculator/amortizationcalculator.h"

#include <QDialog>
#include <QLineEdit>
#include <QtWidgets>

namespace Ui {
class AmortizationCalculatorUI;
}

class AmortizationCalculatorUI : public QDialog
{
    Q_OBJECT

public:
    explicit AmortizationCalculatorUI(QWidget *parent = nullptr);
    ~AmortizationCalculatorUI();

private slots:
    void Calculate();
    void SetInputValues();

private:
    AmortizationCalculator m_amortCalc;
    void SetupCalculator();
};

#endif // AMORTIZATIONUI_H
