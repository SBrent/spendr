#ifndef DOWNPAYMENTUI_H
#define DOWNPAYMENTUI_H

#include "Calculators/DownPaymentCalculator/downpaymentcalculator.h"

#include <QDialog>
#include <QLineEdit>
#include <QtWidgets>

namespace Ui {
class DownPaymentCalculatorUI;
}

class DownPaymentCalculatorUI : public QDialog
{
    Q_OBJECT

public:
    explicit DownPaymentCalculatorUI(QWidget *parent = nullptr);
    ~DownPaymentCalculatorUI();

private slots:
    void Calculate();

private:
    DownPaymentCalculator m_downCalc;
    void SetupCalculator();
};

#endif // DOWNPAYMENTUI_H
