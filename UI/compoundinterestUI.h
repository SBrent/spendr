#ifndef COMPOUNDINTERESTUI_H
#define COMPOUNDINTERESTUI_H

#include "Calculators/CompoundInterestCalculator/compoundinterestcalculator.h"

#include <QDialog>
#include <QLineEdit>
#include <QtWidgets>

namespace Ui {
class CompoundInterestUI;
}

class CompoundInterestUI : public QDialog {
    Q_OBJECT

public:
    explicit CompoundInterestUI(QWidget *parent = nullptr);
    ~CompoundInterestUI();

private slots:
    void Calculate();
    //void SetInputValues();

private:
    CompoundInterestCalculator m_compInterestCalc;
    void SetupCalculator();
};

#endif // COMPOUNDINTERESTUI_H
