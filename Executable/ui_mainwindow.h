/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QPushButton *launchrentvbuy;
    QLabel *label;
    QPushButton *launchdownpayment;
    QPushButton *launchamortization;
    QPushButton *launchcompoundinterest;
    QPushButton *launchpresentvalue;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(444, 240);
        MainWindow->setStyleSheet(QString::fromUtf8(""));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        launchrentvbuy = new QPushButton(centralwidget);
        launchrentvbuy->setObjectName(QString::fromUtf8("launchrentvbuy"));
        launchrentvbuy->setGeometry(QRect(240, 110, 181, 28));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(160, 0, 121, 61));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);
        launchdownpayment = new QPushButton(centralwidget);
        launchdownpayment->setObjectName(QString::fromUtf8("launchdownpayment"));
        launchdownpayment->setGeometry(QRect(240, 70, 181, 28));
        launchamortization = new QPushButton(centralwidget);
        launchamortization->setObjectName(QString::fromUtf8("launchamortization"));
        launchamortization->setGeometry(QRect(20, 70, 181, 28));
        launchcompoundinterest = new QPushButton(centralwidget);
        launchcompoundinterest->setObjectName(QString::fromUtf8("launchcompoundinterest"));
        launchcompoundinterest->setGeometry(QRect(20, 110, 181, 28));
        launchpresentvalue = new QPushButton(centralwidget);
        launchpresentvalue->setObjectName(QString::fromUtf8("launchpresentvalue"));
        launchpresentvalue->setGeometry(QRect(20, 150, 181, 28));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 444, 26));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        launchrentvbuy->setText(QCoreApplication::translate("MainWindow", "Rent vs. Buy Calculator", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "SPENDR ", nullptr));
        launchdownpayment->setText(QCoreApplication::translate("MainWindow", "Down Payment Calculator", nullptr));
        launchamortization->setText(QCoreApplication::translate("MainWindow", "Amortization Calculator", nullptr));
        launchcompoundinterest->setText(QCoreApplication::translate("MainWindow", "Compound Interest Calculator", nullptr));
        launchpresentvalue->setText(QCoreApplication::translate("MainWindow", "Present Value Calculator", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
