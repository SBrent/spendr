#include <stdio.h> 
#include <stdlib.h> 
#include <iostream>
#include <string>
#include "sqlite3.h"

using namespace std;

sqlite3* DB;
char* zErrMsg;  
int rc = 0; 
const char* sql; 
string password;

//Callback function 
static int callback(void *NotUsed, int argc, char **argv, char **azColName) {

   password = argv[0]; 
   return 0;
}

//Connect to database 
void OpenDatabase(){
    rc = sqlite3_open("loginDB.db", &DB); 
}

//Register username and password to the database 
void Register(){

    string username; 
    string password;

    printf("Please enter username: "); 
    cin >> username; 

    printf("Please enter password: ");
    cin >> password; 

    //Try to insert into database
    std::string sql = "INSERT INTO USER (username, password) VALUES ('" + username + "','" + password + "');"; 
    rc = sqlite3_exec(DB, sql.c_str(), callback, 0, &zErrMsg); 

    //Show the sql error number 
    if( rc != SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    } 

    //Username is already in the database. Ask user to enter another username. 
    if (rc == 19){
        printf("Username taken! Please enter a new username.\n");
        Register();
    }

    else {
        fprintf(stdout, "Your username was registered\n");
    }
}

//User can login with name and password. Checked against the database 
void Login(){

    string username; 
    string passwordInput;

    printf("LOGIN\n"); 
    printf("Username: ");
    cin >> username; 
    std::string sql = "SELECT password FROM USER WHERE username = '"+ username + "'"; 
    rc = sqlite3_exec(DB, sql.c_str(), callback, 0, &zErrMsg);  

    printf("Password: "); 
    cin >> passwordInput; 

    while(password != passwordInput){
        printf("Incorrect password!\n");
        printf("Password: "); 
        cin.clear(); 
        cin >> passwordInput; 
    }

    printf("Logged In\n"); 
}

//Show command line menu
void ShowMenu(){

    int userChoice; 
    printf("MENU\n"); 
    printf("1. Register\n"); 
    printf("2. Login\n");
    cin >> userChoice; 
    while(userChoice < 1 || userChoice > 2){
        cin.clear();
        printf("Please choose a valid option "); 
        cin >> userChoice; 
    }

    switch(userChoice){
        case 1:
            Register(); 
            break;  

        case 2: 
            Login();
            break;
    }
}

//Close database connection
void CloseDatabase(){
    sqlite3_close(DB); 
} 
    
    