#include "dbnetworth.hpp"

namespace NetWorth{

double caculateNetWorth(Database & db){
    auto data = db.exec("SELECT * FROM NETWORTH");

    double totalValue = 0;

    for (auto it = data.begin(); it != data.end(); it++){
        double value = (*it)["VALUE"].toDouble();
        totalValue += value;
    }

    return totalValue;
}

QString printAssets(Database & db){
    QString assetsStr = "";
    QString debtsStr = "";
    QString finalStr = "";

    double totalValue = 0;
    auto data = db.exec("SELECT * FROM NETWORTH");

    for (auto it = data.begin(); it != data.end(); it++){
        double value = (*it)["VALUE"].toDouble();
        const QString & name = (*it)["NAME"].toString();
        if (value > 0){
            assetsStr += name + QString("\t\t") + QString::number(value) + QString("\n");
        }else{
            debtsStr += name + QString("\t\t") + QString::number(value) + QString("\n");
        }
        totalValue += value;
    }

    finalStr = QString("Name\t\tValue\n\nAssets:\n\n") + assetsStr + QString("\n----------\nDebts:\n\n") + debtsStr + QString("\n----------\nNet Worth: ") + QString::number(totalValue) + QString("\n");
    return finalStr;
}

void addAsset(Database & db, const QString & name, double value){
    auto query = db.getQuery("INSERT INTO NETWORTH (NAME, VALUE) VALUES (:NAME, :VALUE);");
    query.bindValue(":NAME", name);
    query.bindValue(":VALUE", value);
    db.exec(query);
}

void prepDb(Database & db){
    db.exec("CREATE TABLE NETWORTH("\
            "NAME   TEXT    NOT NULL,"\
            "VALUE  REAL    NOT NULL);", true);
}

}
