#include <QVariant>
#include "database.hpp"

Database::Database(std::string fName){
    openDBFile(fName);
}
void Database::openDBFile(std::string fName){
    db.close();
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QString(fName.c_str()));
    if (!db.open()) qDebug(db.lastError().text().toStdString().c_str());
}

std::vector<std::unordered_map<std::string, QVariant> > Database::exec(std::string string, bool suppress){
    return this->exec(getQuery(string), suppress);
}

std::vector<std::unordered_map<std::string, QVariant> > Database::exec(QSqlQuery query, bool suppress){

    std::vector<std::unordered_map<std::string, QVariant> > rowVector;

    bool res = query.exec();

    if (!res && !suppress) qDebug("Err %s\n", query.lastError().text().toStdString().c_str());

    if (query.isSelect()){

        auto record = query.record();
        int count = 0;

        while (query.next()) {
            rowVector.emplace_back();
            for (int i = 0; i < record.count(); i++){
                rowVector[count][record.fieldName(i).toStdString()] = query.value(i);
            }
            count++;
        }
    }


    return rowVector;
}

QSqlQuery Database::getQuery(){
    return QSqlQuery(db);

}
QSqlQuery Database::getQuery(std::string string){
    QSqlQuery q(db);
    q.prepare(QString(string.c_str()));
    return q;
}
