#ifndef DATABASE_HPP
#define DATABASE_HPP

#include <string>
#include <vector>
#include <unordered_map>
#include <mutex>
#include <string>
#include <QSql>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDebug>

class Database{
private:

    QSqlDatabase db;
    std::mutex ioLock;

public:
    Database(std::string fName);
    void openDBFile(std::string fName);

    std::vector<std::unordered_map<std::string, QVariant> > exec(std::string, bool suppress = false);
    std::vector<std::unordered_map<std::string, QVariant> > exec(QSqlQuery, bool suppress = false);

    QSqlQuery getQuery();
    QSqlQuery getQuery(std::string);
};

#endif
