#include "database.hpp"

#ifndef DB_DBNETWORTH_HPP
#define DB_DBNETWORTH_HPP

namespace NetWorth{

double caculateNetWorth(Database & db);
QString printAssets(Database & db);
void addAsset(Database & db, const QString & name, double value);
void prepDb(Database & db);

}

#endif
